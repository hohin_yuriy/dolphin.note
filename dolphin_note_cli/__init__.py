"""

This is ready to use task tracker with command line interface.
It uses core module as main building block.
To use it print `dolph [any command you want]`.
To get help print `dolph [-h|--help]

Modules:
    * exceptions_interceptor.py
    print_exceptions_pretty_and_log decorator
    catches exceptions, prints it pretty and
    writes exceptions to the log file

    * dfs_printer - makes depth first search
    in project notes hierarhy

    * json_util - gets current user id from config file
"""
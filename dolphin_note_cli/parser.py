"""CLI application entry point.

Functions that calls after entering some commands by user from command line.
"""
from datetime import date

import click
import sys
from click import UsageError
from click_datetime import Datetime

from dolphin_note_cli.manager import CliAppManager
from dolphin_note_library.utils.datetime import get_relative_delta


@click.group(
    "dolph",
    invoke_without_command=True,
    short_help="main console application"
)
@click.pass_context
def cli(ctx):
    if ctx.invoked_subcommand is None:
        click.echo("I was invoked without subcommand")
        click.secho(ctx.get_help())


@cli.group(
    "project",
    invoke_without_command=True,
    short_help="command that helps to work with project"
)
@click.option(
    "--list",
    "-l",
    help="Prints list of projects",
    is_flag=True
)
@click.pass_context
def dolf_project(ctx, list):
    """Project entity commands group

    :param ctx: click.core.Context, dict that consists main command settings
    :param list: bool, optional, flag that outputs all user projects
    calling show_projects function from cli app manager
    """
    if ctx.invoked_subcommand is None:
        if list:
            manager = CliAppManager()
            manager.show_projects()
        else:
            click.echo("I was invoked without subcommand")
            click.secho(ctx.get_help())


@dolf_project.command(
    "add",
    short_help="adds project to your tracker"
)
@click.option(
    "--name",
    type=str,
    required=True,
    help="Project name"
)
def create_project(name):
    """Creates new user project calling create project
    from cli app manager

    :param name: str, required, name of the new project

    """
    manager = CliAppManager()
    manager.create_project(name)


@dolf_project.command(
    "show",
    short_help="shows project"
)
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of project that we want to show"
)
def show_project(id):
    """Shows project info with notes tree
    calling show_project function from
    cli application manager

    :param id: int,required, id of the showing project
    """
    manager = CliAppManager()
    manager.show_project(id)


@dolf_project.command(
    "update",
    short_help="updates project params"
)
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of note that we want to update"
)
@click.option(
    "--name",
    type=str,
    default=None,
    required=False,
    help="Project name"
)
@click.option(
    "--archived/--no-archived",
    required=True,
    default=None
)
def update_project(id, name, archived):
    """Updates user project calling
    update_project function from
    cli application manager

    :param id: int, required, id of the updating project
    :param name: str, optional, new name of the project
    :param archived: optional, new project archive status
    """
    manager = CliAppManager()
    manager.update_project(id, name, archived)


@dolf_project.command(
    "remove",
    short_help="removes project"
)
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of note that we want to remove"
)
def remove_project(id):
    """Removes user project calling
    remove_project function from
    cli application manager

    :param id: int, required, id of the removing project
    """
    manager = CliAppManager()
    manager.remove_project(id)


@dolf_project.command(
    "assign",
    short_help="adds collaborator to project"
)
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of note where we want to assign user"
)
@click.option(
    "--username",
    "-u",
    type=click.STRING,
    required=True,
    default=None,
    help="name of the assigning user"
)
def assign_user_to_project(id, username):
    """Assigns user to project calling
    assign_user_to_project
    :param id: int, required, id of the project
    :param username: str, required, name of the user that we want to assign
    """
    manager = CliAppManager()
    manager.assign_user_to_project(id, username)


@dolf_project.command(
    "deassign",
    short_help="removes collaborator from project"
)
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of note where we want to deassign user"
)
@click.option(
    "--username",
    "-u",
    type=click.STRING,
    required=True,
    default=None,
    help="name of the deassigning user"
)
def remove_assignee_from_project(id, username):
    """Removes assignee from project using
    remove_assignee_from_project

    :param id: int, required, id of the project
    :param username: str, required, name of the user that we want to deassign
    """
    manager = CliAppManager()
    manager.remove_assignee_from_project(id, username)


@cli.group(
    "note",
    short_help="command that helps to work with note"
)
@click.pass_context
def dolf_note(ctx):
    if ctx.invoked_subcommand is None:
        click.echo("I was invoked without subcommand")
        click.secho(ctx.get_help())


@dolf_note.command(
    "add",
    short_help="Add note to project"
)
@click.option(
    "--name",
    type=str,
    help="Paste the note name hear to create note",
    default=None
)
@click.option(
    "--priority",
    "-pr",
    type=click.Choice(["Low", "Normal", "Critical", "High"]),
    default="Normal"
)
@click.option(
    "--project-id",
    "-p",
    type=click.INT,
    default=None,
    help="Id of the project"
)
@click.option("--parent-note-id", "-par", type=click.INT, default=None,
              help="Id of the parent note")
def add_note(name, priority, parent_note_id, project_id):
    """
    Adds note to project calling
    add_note function from
    cli application manager

    :param name: str, required, name of the note
    :param priority: str, optional,
    default "Normal"
    :param parent_note_id:
    :param project_id:
    """
    manager = CliAppManager()
    manager.add_note(
        name,
        priority,
        parent_note_id=parent_note_id,
        project_id=project_id
    )


@dolf_note.command(
    "update",
    short_help="Updates note!"
)
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of note that we want to update"
)
@click.option(
    "--name",
    type=str,
    help="New name of the note",
    default=None
)
@click.option(
    "--priority",
    "-pr",
    type=click.Choice(
        [
            "Low",
            "Normal",
            "Critical",
            "High"
        ]
    ),
    default=None
)
@click.option(
    "--parent-note-id",
    "-p",
    type=click.INT,
    default=None,
    help="Id of the parent note"
)
@click.option(
    "--due",
    "-due",
    type=Datetime("%d-%m-%Y"),
    default=None,
    help="Note deadline"
)
def update_note(
        id,
        name=None,
        priority=None,
        parent_note_id=None,
        due=None
):
    """Updates note calling
    add_note function from
    cli application manager

    :param name: str, required, name of the note
    :param priority: str, optional,
    default "Normal"
    :param parent_note_id: int, optional
    if we use without project_id
    id of the parent note
    :param project_id: int, optional
    if we use without parent_note_id
    id of the project_id
    """
    manager = CliAppManager()
    manager.update_note(
        id,
        name=name,
        priority=priority,
        parent_note_id=parent_note_id,
        due_date=due
    )


@dolf_note.command("show")
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of note that we want to show"
)
def show_note(id):
    """Shows information about note calling
    show_note finction from cli application manager

    :param id: int, required, id of the showing note
    """
    manager = CliAppManager()
    manager.show_note(id)


@dolf_note.command("assign")
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of the note to assign user to it"
)
@click.option(
    "--username",
    "-u",
    type=click.STRING,
    required=True,
    default=None,
    help="name of the assigning user"
)
def assign_user_to_note(id, username):
    manager = CliAppManager()
    manager.assign_user_to_note(id, username)


@dolf_note.command("deassign")
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of the note to deassign user from it"
)
@click.option(
    "--username",
    "-u",
    type=click.STRING,
    required=True,
    default=None,
    help="name of the deassigning user"
)
def deassign_user_from_note(id, username):
    manager = CliAppManager()
    manager.deassign_user_to_note(id, username)


@cli.group(
    "label",
    invoke_without_command=True,
    short_help="command that helps to work with label"
)
@click.option(
    "--list",
    "-l",
    help="Prints list of labels",
    is_flag=True
)
@click.pass_context
def dolf_label(ctx, list):
    if ctx.invoked_subcommand is None:
        if list:
            manager = CliAppManager()
            manager.show_labels()
        else:
            click.echo("I was invoked without subcommand")
            click.secho(ctx.get_help())


@dolf_label.command("add")
@click.option(
    "--name",
    type=click.STRING,
    required=True,
    default=None,
    help="name of the new label"
)
def add_label(name):
    manager = CliAppManager()
    manager.add_label(name)


@dolf_label.command("assign")
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of the note where label is situated"
)
@click.option(
    "--name",
    type=click.STRING,
    required=True,
    default=None,
    help="name of the label that we want to assign"
)
def assign_label_to_note(id, name):
    manager = CliAppManager()
    manager.assign_label_to_note(id, name)


@dolf_label.command("deassign")
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of the note where label is situated")
@click.option(
    "--name",
    type=click.STRING,
    required=True,
    default=None,
    help="name of the removing label from note"
)
def remove_note_label(id, name):
    manager = CliAppManager()
    manager.remove_note_label(id, name)


@dolf_label.command("remove")
@click.option(
    "--name",
    type=click.STRING,
    required=True,
    default=None,
    help="name of the removing label"
)
def remove_label(name):
    manager = CliAppManager()
    manager.remove_label(name)


@cli.group(
    "notification",
    invoke_without_command=True,
    short_help="command that helps to work with notifications"
)
@click.pass_context
def dolph_notification(ctx):
    if ctx.invoked_subcommand is None:
        click.echo("I was invoked without subcommand")
        click.secho(ctx.get_help())


@dolph_notification.command("add")
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of the note what we want to notify"
)
@click.option(
    "--date",
    "-t",
    type=click.STRING,
    required=False,
    default=None,
    help="time when we want to notify about event"
)
@click.option(
    "--delta-type",
    "-dt",
    type=click.Choice(
        [
            "DAYS",
            "WEEKS",
            "MONTHS",
            "YEARS"
        ]
    ),
    required=False,
    default="DAYS"
)
def add_notification(id, date, delta_type, quantity):
    """Adds notification to note
    calling

    :param id:
    :param time:
    :param delta_type:
    :return:
    """
    delta = get_relative_delta(delta_type, quantity)
    manager = CliAppManager()
    manager.add_notification(id, date, delta)


@dolph_notification.command("remove")
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of the notification to remove"
)
def remove_notification(id):
    """Remove notification
    calling remove_notification function
    from cli application manager

    :param id: int, required, id of notification entity
    """
    manager = CliAppManager()
    manager.remove_notification(id)


@dolph_notification.command("check")
@click.option(
    "--periods-type",
    "-pt",
    type=click.Choice(
        [
            "DAYS",
            "WEEKS",
            "MONTHS",
            "YEARS"
        ]
    ),
    default="DAYS"
)
@click.option(
    "--periods-number",
    type=click.INT,
    default=1
)
def check_notifications(periods_type, periods_number):
    """Checks notifications and shifts notification
    dates in case of notification repetition

    :param periods_type:
    :param periods_number:
    """
    manager = CliAppManager()
    manager.check_notifications(
        period_type=periods_type,
        periods_number=periods_number
    )


@cli.group(
    "profile",
    invoke_without_command=True,
    short_help="command that helps to work with profile"
)
@click.pass_context
def dolf_profile(ctx):
    if ctx.invoked_subcommand is None:
        click.echo("I was invoked without subcommand")
        click.secho(ctx.get_help())


@dolf_profile.command("create")
@click.option(
    "--username",
    type=str,
    required=True,
    help="Username of creating user"
)
@click.option(
    "--email",
    type=str,
    default="",
    help="Email of creating user"
)
def create_user(username, email):
    manager = CliAppManager()
    manager.create_user(username, email)


@dolf_profile.command("change")
@click.option(
    "--username",
    type=str,
    required=True,
    help="username of changing user"
)
def change_user(username):
    """Changes executor in cli
    application calling change_user
    function from cli application manager

    :param username: str, required,  name of the user that we want to change
    """
    manager = CliAppManager()
    manager.change_user(username)


@cli.group(
    "schedule",
    invoke_without_command=True,
    short_help="command that helps to work with scheduler"
)
@click.pass_context
def dolph_scheduler(ctx):
    """ Command group for Schedule and NoteTemplate entities

    :param ctx: click.core.Context, dict that consists main command settings
    """
    if ctx.invoked_subcommand is None:
        click.echo("I was invoked without subcommand")
        click.secho(ctx.get_help())


@dolph_scheduler.command(
    "add",
    short_help="Add note to project"
)
@click.option(
    "--period-type",
    "-pt",
    type=click.Choice(
        [
            "DAYS",
            "WEEKS",
            "MONTHS",
            "YEARS"
        ]
    ),
    default="DAYS",
    help="Type of the repetition_period"
)
@click.option(
    "--periods-number",
    type=click.INT,
    default=0
)
@click.option(
    "--start-date",
    "-start",
    type=Datetime(format='%d-%m-%Y'),
    default=date.today(),
    help="Start date of the scheduler"
)
@click.option(
    "--end-date",
    "-end",
    type=Datetime(format='%d-%m-%Y'),
    default=None,
    help="End date of the scheduler"
)
@click.option(
    "--note-id",
    "-i",
    type=click.INT,
    default=None,
    help="Id of the parent note"
)
def add_recurring_note_creation(
        start_date,
        end_date,
        period_type,
        periods_number,
        note_id
):
    """Calls add_recurring_note_creation from
    cli application manager and
    make notes create from start date to
    end date every delta

    :param start_date: str, required, start date
    of the creating notes in %Y-%M-%D format
    :param end_date: str, optional, end date
    of the creating notes in %Y-%M-%D format
    :param period_type: str, choice from "DAYS", "WEEKS",
    "MONTHS", "YEARS", unit of measurement of periods number
    :param periods_number: quantit
    """
    manager = CliAppManager()
    manager.add_recurring_note_creation(
        start_date=start_date,
        end_date=end_date,
        period_type=period_type,
        periods_number=periods_number,
        note_id=note_id
    )


@dolph_scheduler.command("remove")
@click.option(
    "--id",
    "-i",
    type=click.INT,
    required=True,
    help="Id of the removing scheduler"
)
def remove_scheduler(id):
    """Calls remove_scheduler function
    from cli application manager
    and removes repeatable notes
    creation

    :param id: int, required, id of Schedule entity
    """
    manager = CliAppManager()
    manager.remove_scheduler(id)


@dolph_scheduler.command("check")
def show_schedulers():
    """Checks user schedules,
    shows it, and create notes
    from note templates if
    we must to create it
    """
    manager = CliAppManager()
    manager.show_schedulers()


def entry_point():
    """Entry point of the application"""
    try:
        cli(standalone_mode=False)
        sys.exit(0)
    except UsageError as e:
        print(e)
        with click.Context(cli) as ctx:
            if e.format_message().startswith("No such command"):
                print(ctx.get_help())
        sys.exit(2)

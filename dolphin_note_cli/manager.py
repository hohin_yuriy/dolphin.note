import click

import dolphin_note_cli.utils.printers as printers
import dolphin_note_library.commands.label as label_commands
import dolphin_note_library.commands.note as note_commands
import dolphin_note_library.commands.notification as notification_commands
import dolphin_note_library.commands.project as project_commands
import dolphin_note_library.commands.schedule as sheduler_commands
from dolphin_note_cli.utils.exceptions_interceptor import print_exceptions_pretty_and_log
from dolphin_note_cli.utils.dfs_printer import print_notes_tree
from dolphin_note_cli.utils.json_util import (
    get_config,
    set_config,
)
from dolphin_note_library.commands.user import (
    get_user_by_id,
    add_user,
    get_user_by_username
)
from dolphin_note_library.database import Database
from dolphin_note_library.models.note import NotePriority
from dolphin_note_library.utils.datetime import (
    get_datetime_from_string,
    get_relative_delta,
)
from dolphin_note_library.utils.exceptions import BadUsageException


class CliAppManager:
    """
        Class connects dolphin_note_library library commands with dolphin_note_cli app,
        provides commands execuption and printing of the result to
        the command line
    """
    def __init__(self):
        """
            initialize session with database and gets current user
        """
        database = Database()
        self.session = database.get_session()
        self.user_id = get_config('current_user')

    @print_exceptions_pretty_and_log
    def show_projects(self):
        """
            This method finds user projects and prints it
        """
        if list:
            current_projects = project_commands.get_all_user_projects(
                self.session,
                self.user_id,
                is_archived=False
            )
            archived_projects = project_commands.get_all_user_projects(
                self.session,
                self.user_id,
                is_archived=True
            )
            assigned_projects = project_commands.get_all_assigned_projects(
                self.session,
                self.user_id
            )

            click.secho('Current projects:')
            printers.print_projects(current_projects)

            click.secho('Archived projects:')
            printers.print_projects(archived_projects)

            click.secho('Assigned projects:')
            printers.print_projects(assigned_projects)

    @print_exceptions_pretty_and_log
    def create_project(self, name):
        """
            Creates project and prints it
        """
        project = project_commands.add_project(
            self.session,
            self.user_id,
            name
        )
        if project:
            printers.print_project(project)

    @print_exceptions_pretty_and_log
    def show_project(self, project_id):
        """
            Prints project with its notes hierarhy
        """
        project = project_commands.get_project_by_id(
            self.session,
            self.user_id,
            project_id
        )
        if project:
            printers.print_project(project)
            click.secho("Notes: ")
            print_notes_tree(
                self.session,
                self.user_id,
                project_id
            )

        else:
            click.secho('Can\'t show project!')

    @print_exceptions_pretty_and_log
    def update_project(self, project_id, name, archived):
        """
            Updates project data
        """
        project_commands.update_project(
            self.session,
            self.user_id,
            project_id=project_id,
            project_name=name,
            project_is_archived=archived
        )
        project = project_commands.get_project_by_id(
            self.session,
            self.user_id,
            project_id
        )
        if project:
            printers.print_project(project)

    @print_exceptions_pretty_and_log
    def remove_project(self, project_id):
        """
            Removes project
        """
        result = project_commands.remove_project(
            self.session,
            self.user_id,
            project_id
        )
        if result:
            click.secho('Project deleted!')

    @print_exceptions_pretty_and_log
    def assign_user_to_project(self, project_id, username):
        """
             Assigns user to project
        """
        project_collaborator = project_commands.assign_user_to_project(
            self.session,
            self.user_id,
            id,
            username
        )
        if project_collaborator:
            click.secho(
                'User {} assigned to project with id {}!'.format(
                    username,
                    id
                )
            )

    @print_exceptions_pretty_and_log
    def remove_assignee_from_project(self, project_id, username):
        """
            Removes user assignation to project
        """
        project_collaborator = project_commands.remove_project_assignee(
            self.session,
            self.user_id,
            project_id,
            username
        )
        if project_collaborator:
            click.secho(
                'User {} deassigned from project with id {}!'.format(
                    username, id
                )
            )

    @print_exceptions_pretty_and_log
    def add_note(self, name, priority, project_id=None, parent_note_id=None):
        """
            Creates note
        """
        if project_id and parent_note_id:
            raise BadUsageException("You can write only or parent note or project!")
        note = note_commands.add_note(
            self.session,
            self.user_id,
            name=name,
            priority=NotePriority(priority),
            parent_note_id=parent_note_id,
            project_id=project_id
        )
        if note:
            click.secho('Note added with id {}!'.format(note.id))

    @print_exceptions_pretty_and_log
    def update_note(
            self,
            note_id,
            name=None,
            priority=None,
            archive=None,
            parent_note_id=None,
            due_date=None
    ):
        """
            Updates note data
        """
        if due_date:
            due_date = due_date.date()

        note_commands.update_note(
            self.session,
            self.user_id,
            note_id,
            name=name,
            archive=archive,
            parent_note_id=parent_note_id,
            due_date=due_date,
            priority=priority
        )
        note = note_commands.get_note_by_id(
            self.session,
            self.user_id,
            note_id
        )
        if note:
            printers.print_note(note)

    @print_exceptions_pretty_and_log
    def show_note(self, note_id):
        """
            Prints note with its collaborators
        """
        note = note_commands.get_note_by_id(self.session, self.user_id, note_id)
        if note:
            printers.print_note(note)

            project_collaborators = project_commands.get_all_project_assignees(
                self.session,
                self.user_id,
                note.project_id
            )
            note_collaborators = note_commands.get_note_assignees(
                self.session,
                self.user_id,
                note.id
            )

            click.secho('collaborators:', fg='red')
            click.secho('by project:', fg='green')
            for project_collaborator in project_collaborators:
                user = get_user_by_id(
                    self.session,
                    project_collaborator.member_id
                )
                printers.print_user(user)

            click.secho('by note:', fg='green')
            for note_collaborator in note_collaborators:
                user = get_user_by_id(self.session, note_collaborator.collaborator_id)
                printers.print_user(user)

    @print_exceptions_pretty_and_log
    def assign_user_to_note(self, note_id, username):
        """
            Adds collaborator to note
        """
        collaborator = note_commands.assign_user_to_note(self.session, self.user_id, note_id, username)
        if collaborator:
            pass

    @print_exceptions_pretty_and_log
    def deassign_user_to_note(self, note_id, username):
        """
            Removes note collaborator
        """
        collaborator = note_commands.remove_all_note_assignees(
            self.session,
            self.user_id,
            note_id,
            username
        )
        if collaborator:
            click.secho("Deleted note collaboration")

    @print_exceptions_pretty_and_log
    def add_label(self, name):
        """
            Adds label
        """
        label = label_commands.add_label(
            self.session,
            self.user_id,
            name
        )
        if label:
            printers.print_label(label)

    @print_exceptions_pretty_and_log
    def show_labels(self):
        """
            Adds label
        """
        labels = label_commands.get_user_labels(
            self.session,
            self.user_id
        )
        printers.print_labels(labels)

    @print_exceptions_pretty_and_log
    def assign_label_to_note(self, note_id, label_name):
        """
            Links note with label
        """
        label = label_commands.get_label_by_name(
            self.session,
            self.user_id,
            label_name)

        note_label = label_commands.assign_label_to_note(
            self.session,
            self.user_id,
            note_id=note_id,
            label_id=label.id
        )
        if note_label:
            click.secho(
                'Assigned label {} to note with id {}'.format(
                    label.name, note_id
                )
            )

    @print_exceptions_pretty_and_log
    def remove_note_label(self, note_id, label_name):
        """
            Removes note-label connection
        """
        label = label_commands.get_label_by_name(self.session, self.user_id, label_name)
        label_commands.remove_note_label(
            self.session,
            self.user_id,
            note_id=note_id,
            label_id=label.id
        )
        click.secho("Label {} removed from note {}".format(label_name, note_id))

    @print_exceptions_pretty_and_log
    def remove_label(self, label_name):
        """
            Removes label
        """
        result = label_commands.remove_label(
            self.session,
            self.user_id,
            label_name=label_name
        )
        if result:
            click.secho(
                'Label with name {} was removed'.format(
                        label_name
                )
            )

    @print_exceptions_pretty_and_log
    def add_notification(self, note_id, date, delta):
        """
            Adds note notification
        """

        date = get_datetime_from_string(date)
        notification = notification_commands.add_time_notification(
            self.session,
            self.user_id,
            note_id=note_id,
            date=date,
            delta=delta
        )
        printers.print_notification(notification)

    @print_exceptions_pretty_and_log
    def remove_notification(self, notification_id):
        """
            Removes note notification
        """
        result = notification_commands.delete_time_notification(
            self.session,
            self.user_id,
            notification_id
        )
        if result:
            print("Notification removed")

    @print_exceptions_pretty_and_log
    def check_notifications(self, period_type, periods_number):
        """
            Checks notes notifications, deadline and updates it
        """
        delta = get_relative_delta(period_type, periods_number)
        date_time_notes, notifications = notification_commands.check_notifications(
            self.session,
            self.user_id,
            delta
        )
        printers.print_notes(date_time_notes)
        printers.print_notifications(notifications)

    @print_exceptions_pretty_and_log
    def create_user(self, username, email):
        """
            Creates new user to tracker
        """
        user = add_user(self.session, username, email)
        set_config('current_user', user.id)

    @print_exceptions_pretty_and_log
    def change_user(self, username):
        """
        Change current user
        """
        user = get_user_by_username(self.session, username)
        set_config('current_user', user.id)

    @print_exceptions_pretty_and_log
    def add_recurring_note_creation(
            self,
            start_date,
            end_date,
            period_type,
            periods_number,
            note_id
    ):
        if start_date:
            start_date = get_datetime_from_string(start_date)
        if end_date:
            end_date = end_date.date()
        delta = get_relative_delta(period_type, periods_number)
        schedule = sheduler_commands.add_schedule(
            self.session,
            self.user_id,
            delta=delta,
            start_date=start_date,
            end_date=end_date,
            note_id=note_id
        )

        if schedule:
            click.secho(
                "We will create a note with id {} every {} since {} and till {}".format(
                    note_id,
                    delta,
                    start_date,
                    end_date
                )
            )

    @print_exceptions_pretty_and_log
    def show_schedulers(self):
        """
            Shows all user schedulers
        """
        schedulers = sheduler_commands.get_all_user_schedules(
            self.session,
            self.user_id
        )
        sheduler_commands.create_user_notes_from_schedules(
            self.session,
            self.user_id
        )
        for sheduler in schedulers:
            click.secho(
                "{} every {} since {} and till {}".format(
                    sheduler.note.name,
                    sheduler.delta,
                    sheduler.start_date,
                    sheduler.end_date
                )
            )

    @print_exceptions_pretty_and_log
    def remove_scheduler(self, scheduler_id):
        """
            Removes scheduler
        """
        sheduler_commands.remove_schedule(
            self.session,
            self.user_id,
            scheduler_id
        )
        click.secho("Scheduler removed!")







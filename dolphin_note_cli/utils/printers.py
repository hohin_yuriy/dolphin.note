import click

"""
    This module provides pretty printings of tracker
    entities to the command line
"""


def print_project(project):
    """
        Prints project to the command line
    """
    click.secho('project id: {}'.format(project.id), fg='green')
    click.secho('project name: {}'.format(project.name), fg='green')
    click.secho('is archived: {}'.format(str(project.is_archived)), fg='red')
    click.echo()


def print_projects(projects):
    """
        Prints list of project to the command line
    """
    for project in projects:
        print_project(project)
        click.echo()


def print_note(note):
    """
        Prints note to the command line
    """
    click.secho('id: {}'.format(note.id), fg='blue')
    click.secho('project: id: {} name: {}'.format(note.project.id, note.project.name), fg='red')
    click.secho('name: {}'.format(note.name), fg='red')
    click.secho('archive: {}'.format(note.is_archived), fg='red')
    click.secho('parent note id {}:'.format(note.parent_note_id), fg='red')
    click.secho('due: {}'.format(note.due_date), fg='red')
    click.secho('priority: {}'.format(note.priority.value), fg='red')


def print_notes(notes):
    """
        Prints list of notes to the command line
    """
    for note in notes:
        print_note(note)


def print_notification(notification):
    """
        Prints notification to the command line
    """
    click.secho(
        'notification id {} note id:{} name:{}  due: {} reccuring: {}'.format(
            notification.id,
            notification.note.id,
            notification.note.name,
            notification.date,
            notification.recurring_type
        )
    )


def print_notifications(notifications):
    """
        Prints list of notifications to the command line
    """
    for notification in notifications:
        print_notification(notification)


def print_label(label):
    """
        Prints label to the command line
    """
    click.secho('id: {} name: {}'.format(label.id, label.name))


def print_labels(labels):
    """
        Prints labels list to the command line
    """
    for label in labels:
        print_label(label)


def print_user(user):
    """
        Prints user to the command line
    """
    click.secho('id: {}'.format(user.id), fg='blue')
    click.secho('name: {}'.format(user.username), fg='blue')


def print_users(users):
    """
        Prints list of users
    """
    for user in users:
        print_user(user)


def print_project_collaborator(project_collaborator):
    """
        Prints project collaborator to the command line
    """
    click.secho('id: {}'.format(project_collaborator.id), fg='blue')
    click.secho('user id: {}'.format(project_collaborator.username), fg='blue')
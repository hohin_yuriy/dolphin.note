import json

from dolphin_note_cli.config import CLI_USER_CONFIG_PATH


def get_config(field):
    """
    GETS COFIG VALUE FROM CLI CONFIG FILE
    :param field: name of the resources string
    :return: int
    """
    with open(CLI_USER_CONFIG_PATH) as f:
        data = json.load(f)
        return data[field]


def set_config(field, info):
    """
       WRITES CURRENT USER COFIG VALUE FROM CLI CONFIG FILE
       :param field: name of the resources string
       :param info: new resources data of the field
       :return: int
    """

    with open(CLI_USER_CONFIG_PATH, "r+") as f:
        data = json.load(f)
        data[field] = info

        f.seek(0)  # rewind
        json.dump(data, f)
        f.truncate()



"""
    This module contains function
    that prints project note tree hierarhy
    and depth first search for tree traversal
    of the notes hierarhy
"""


def print_notes_tree(session, user_id, project_id):
    """
    Prints note hierarhy to the console
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the project where notes is situated
    """
    from dolphin_note_library.commands.note import get_all_project_root_notes
    root_notes = get_all_project_root_notes(session, user_id, project_id)
    for root_note in root_notes:
        """
            tabs for pretty printing of the tree
            node
            [ tab ]child node
            [ tab ][ tab ] child node
            [ tab ][ tab ] child node  
        """
        tabs = ""
        dfs(root_note, tabs)


def dfs(note, tabs):
    """
    Depth First Search Printer
    :param note: note object
    :param tabs: number of tabs for pretty printing
    """
    import click
    click.secho("{}id: {} name: {}".format(tabs, note.id, note.name), fg='yellow')
    tabs += "   "
    for subnote in note.subnotes:
        dfs(subnote, tabs)


class Heap:
    def __init__(self):
        self.heap = []
        self.size = 0

    def heapify(self, i):
        while True:
            left_child = i * 2 + 1
            right_child = i * 2 + 2
            largest_child = i

            if self.heap[left_child] > self.heap[largest_child]\
                    and left_child < self.size:
                largest_child = left_child

            if self.heap[right_child] > self.heap[largest_child] \
                    and right_child < self.size:
                largest_child = right_child

            if largest_child == i:
                break

            temp = self.heap[i]
            self.heap[i] = self.heap[largest_child]
            self.heap[largest_child] = temp
            i = largest_child

    def insert(self, item):
        self.heap.append(item)
        self.size += 1
        self.heapify(self.size)


def ranges(words):
    result_set = []
    current = []
    for word in words:
        f


import warnings
from functools import wraps

import click
import sys
from sqlalchemy.exc import (
    SQLAlchemyError,
    SAWarning,
)
from sqlalchemy.orm.exc import NoResultFound

from dolphin_note_cli.config import (
    LOG_LEVEL,
    LOG_FORMAT,
    LOG_DIRECTORY,
    LOGGER_NAME
)
from dolphin_note_library.utils.exceptions import BadUsageException
from dolphin_note_library.utils.logging import (
    configure_logging,
    get_logger,
)


def print_exceptions_pretty_and_log(func):
    """
    Wraps the function, prints exception in one line if it raises
    and logs it
    :param func: wrapping function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        configure_logging(
            logger_name=LOGGER_NAME,
            level=LOG_LEVEL,
            log_directory=LOG_DIRECTORY,
            log_format=LOG_FORMAT
        )
        logger = get_logger(LOGGER_NAME)
        try:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=SAWarning)

                logger.info("Called func: {}".format(func.__name__))
                logger.debug("Args: {0}, \n\tKwargs: {1}".format(args, kwargs))
                result = func(*args, **kwargs)
                logger.debug("{0} returned: {1}".format(func.__name__, result))
                return result
        except NoResultFound as e:
            logger.error(str(e))
            click.secho("Can't find data with this key arguments", fg='red')
            sys.exit(1)
        except SQLAlchemyError as e:
            logger.error(str(e))
            click.secho("Can't provide this operation in database", fg='red')
        except BadUsageException as e:
            logger.error(str(e))
            click.secho(str(e), fg="red")
        except PermissionError as e:
            logger.error(str(e))
            click.secho("Permission denided!", fg='red')
            sys.exit(1)
        except Exception as e:
            logger.error(str(e))
            click.secho(str(e))
            sys.exit(1)

    return wrapper

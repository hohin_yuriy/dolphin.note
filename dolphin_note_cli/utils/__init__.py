"""

    This packege represents some useful utils
    for normal work of the parser
    * decorators.py
    print_exceptions_pretty_and_log decorator
    catches exceptions, prints it pretty and
    writes exceptions to the log file

    * dfs_printer - makes depth first search
    in project notes hierarhy

    * json_util - gets current user id from config file

"""
# -*- coding: UTF-8 -*-

"""
    This module configure dolphin_note_cli library of the project
    (c) Hohin Yuri, 2018
"""

import os


CLI_APP_HOME = os.path.dirname(os.path.abspath(__file__))


# cli user resources
CLI_USER_CONFIG_FILE = "current_user.json"
CLI_CONFIG_FOLDER = "resources"
CLI_CONFIG_FOLDER_PATH = os.path.join(CLI_APP_HOME, CLI_CONFIG_FOLDER)
CLI_USER_CONFIG_PATH = os.path.join(CLI_CONFIG_FOLDER_PATH, CLI_USER_CONFIG_FILE)


# log config

LOGGER_NAME = "__DOLPHIN_NOTE_CLI_SUPER_LOGGER__"
LOG_FORMAT = ("(%(process)d/%(threadName)s) "
              "Dolph.Note.Cli:%(levelname)s:%(message)s:(%(asctime)s)")

LOG_FILE_NAME = "dolf.log"
LOG_FILE_FOLDER_NAME = "log"
LOG_DIRECTORY = os.path.join(CLI_CONFIG_FOLDER_PATH, LOG_FILE_FOLDER_NAME)
LOG_LEVEL = "ERROR"
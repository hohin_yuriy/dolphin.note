from dolphin_note.views.note import add_note
from dolphin_note_library.commands.project import add_project
from dolphin_note_library.models.note import Note
from tests.tests_set_up import (
    DolpinNoteTestCase,
    NAME,
    DUE_DATE,
    START_DATE,
    END_DATE,
    DELTA,
)
import dolphin_note_library.commands.schedule as scheduler_commands


class TestSchedule(DolpinNoteTestCase):
    def setUp(self):
        super().setUp()
        self.project = add_project(
            self.session,
            self.main_user.id,
            NAME,
        )
        self.note = add_note(
            self.session,
            self.main_user.id,
            name=NAME,
            project_id=self.project.id,
            due_date=DUE_DATE
        )
        self.schedule = scheduler_commands.add_schedule(
            self.session,
            self.main_user.id,
            delta=DELTA,
            start_date=START_DATE,
            end_date=END_DATE,
            note_id=self.note.id
        )

    def test_schedule(self):
        self.assertEqual(self.schedule.start_date, START_DATE)
        self.assertEqual(self.schedule.end_date, END_DATE)
        self.assertEqual(self.schedule.delta, DELTA)

    def add_note_from_template(self):
        self.note_from_template = scheduler_commands.add_note_copy(
            self.session,
            self.main_user.id,
            self.note
        )
        self.assertTrue(isinstance(self.note_from_template, Note))
        self.assertEqual(self.note_from_template.name, NAME)
        self.assertEqual(self.note_from_template.project, self.project)
        self.assertEqual(self.note_from_template.due_date, DUE_DATE)

    def test_get_schedule_by_id(self):
        schedule = scheduler_commands.get_schedule_by_id(
            self.session,
            self.main_user.id,
            self.schedule.id
        )
        self.assertEqual(schedule, self.schedule)

    def test_get_all_user_schedulers(self):
        user_schedulers = scheduler_commands.get_all_user_schedules(
            self.session,
            self.main_user.id
        )
        self.assertTrue(self.schedule in user_schedulers)

    def test_remove_schedule(self):
        result = scheduler_commands.remove_schedule(
            self.session,
            self.main_user.id,
            self.schedule.id
        )
        self.assertTrue(result)

    def test_create_user_notes_from_schedules(self):
        notes = scheduler_commands.create_user_notes_from_schedules(
            self.session,
            self.main_user.id
        )
        self.assertTrue(notes)
        for note in notes:
            self.assertEqual(note.name, NAME)
            self.assertEqual(note.project, self.project)
            self.assertEqual(note.due_date, DUE_DATE)
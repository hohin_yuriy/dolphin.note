import unittest

import dolphin_note_library.commands.project as project_commands
from tests.tests_set_up import (
    DolpinNoteTestCase,
    NAME,
    COLLABORATOR_USERNAME
)


class ProjectTest(DolpinNoteTestCase):
    def setUp(self):
        super().setUp()
        self.project = project_commands.add_project(
            self.session,
            self.main_user.id,
            NAME
        )

    def test_add_project(self):
        self.assertEquals(NAME, self.project.name)
        self.assertEquals(NAME, self.project.name)

    def test_get_project_by_id(self):
        project = project_commands.get_project_by_id(
            self.session,
            self.project.creator_id,
            self.project.id
        )
        self.assertEquals(project, self.project)

    def test_get_all_assigned_projects(self):
        project_collaborator = project_commands.assign_user_to_project(
            self.session,
            self.main_user.id,
            self.project.id,
            COLLABORATOR_USERNAME
        )
        assigned_projects = project_commands.get_all_assigned_projects(
            self.session,
            self.second_user.id
        )
        print(assigned_projects)
        self.assertTrue(self.project in assigned_projects)

    def test_get_get_all_project_assignee(self):
        project_collaborator = project_commands.assign_user_to_project(
            self.session,
            self.main_user.id,
            self.project.id,
            COLLABORATOR_USERNAME
        )
        project_assignees = project_commands.get_all_project_assignees(
            self.session,
            self.main_user.id,
            self.project.id
        )
        self.assertTrue(project_collaborator in project_assignees)

    def test_get_all_user_projects(self):
        projects = project_commands.get_all_user_projects(
            self.session,
            self.main_user.id
        )
        self.assertTrue(self.project in projects)

    def test_update_project(self):
        project = project_commands.update_project(
            self.session,
            self.project.creator_id,
            self.project.id,
            project_name="SUPERKS"
        )
        self.assertEquals("SUPERKS", self.project.name)

    def test_remove_project_assignee(self):
        project_collaborator = project_commands.assign_user_to_project(
           self.session,
           self.main_user.id,
           self.project.id,
           COLLABORATOR_USERNAME
        )
        print(self.project)
        result = project_commands.remove_project_assignee(
           self.session,
           self.project.creator_id,
           self.project.id,
           COLLABORATOR_USERNAME
        )
        self.assertTrue(result)

    def test_assign_user_to_project(self):
        self.project_collaborator = project_commands.assign_user_to_project(
            self.session,
            self.main_user.id,
            self.project.id,
            COLLABORATOR_USERNAME
        )
        self.assertEquals(
            self.project_collaborator.project_id,
            self.project.id
        )
        self.assertEquals(
            self.project_collaborator.member_id,
            self.second_user.id
        )

    def test_remove_project(self):
        result = project_commands.remove_project(
            self.session,
            self.project.creator_id,
            self.project.id
        )
        self.assertTrue(result)


def main():
    unittest.main()


if __name__ == '__main__':
    main()
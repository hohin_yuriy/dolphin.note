import unittest

import dolphin_note_library.commands.label as label_commands
from dolphin_note_library.commands.note import add_note
from dolphin_note_library.commands.project import add_project
from tests.tests_set_up import (
    DolpinNoteTestCase,
    NAME,
    DUE_DATE,
)


class LabelsTest(DolpinNoteTestCase):
    def setUp(self):
        super().setUp()
        self.label = label_commands.add_label(
            self.session,
            self.main_user.id,
            NAME
        )

    def test_add_label(self):
        self.assertEqual(self.label.name, NAME)
        self.assertEqual(self.label.creator_id, self.main_user.id)

    def test_get_label_by_name(self):
        label = label_commands.get_label_by_name(
            self.session,
            self.main_user.id,
            self.label.name
        )
        self.assertEqual(label, self.label)

    def test_get_label_by_id(self):
        label = label_commands.get_label_by_id(
            self.session,
            self.main_user.id,
            self.label.id
        )
        self.assertEqual(label, self.label)

    def test_get_user_labels(self):
        labels = label_commands.get_user_labels(
            self.session,
            self.main_user.id
        )
        self.assertTrue(self.label in labels)

    def test_assign_label_to_note(self):
        self.project = add_project(
            self.session,
            self.main_user.id,
            NAME
        )
        self.note = add_note(
            self.session,
            self.main_user.id,
            name=NAME,
            project_id=self.project.id,
            due_date=DUE_DATE
        )
        self.note_label = label_commands.assign_label_to_note(
            self.session,
            self.main_user.id,
            self.note.id,
            self.label.id
        )
        self.assertEqual(self.note_label.label, self.label)
        self.assertEqual(self.note_label.note, self.note)

    def test_remove_note_label(self):
        self.project = add_project(
            self.session,
            self.main_user.id,
            NAME,
        )
        self.note = add_note(
            self.session,
            self.main_user.id,
            name=NAME,
            project_id=self.project.id,
            due_date=DUE_DATE
        )
        self.note_label = label_commands.assign_label_to_note(
            self.session,
            self.main_user.id,
            self.note.id,
            self.label.id
        )
        result = label_commands.remove_note_label(
            self.session,
            self.main_user.id,
            self.note.id,
            self.label.id
        )
        self.assertTrue(result)

    def test_remove_label(self):
        result = label_commands.remove_label(
            self.session,
            self.main_user.id,
            self.label.name
        )
        self.assertTrue(result)


def main():
    unittest.main()


if __name__ == '__main__':
    main()
import unittest
from datetime import datetime, timedelta

from dolphin_note_library.commands.user import add_user
from dolphin_note_library.database import Database

# constants that used in tests
NAME = "KSENIA"
DUE_DATE = datetime(year=2018, month=9, day=21)
CREATOR_USERNAME = "YURY"
CREATOR_EMAIL = "A@A.COM"
COLLABORATOR_USERNAME = "KS"
COLLABORATOR_EMAIL = "B@B.COM"
NEW_POSITION = 1
START_DATE = datetime.today() - timedelta(days=8)
END_DATE = datetime.today() + timedelta(days=3)
DELTA = timedelta(days=5)


class DolpinNoteTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # init database connection to all tests in class
        cls.database = Database(is_memory=True, db_file_name="db_test")

    def setUp(self):
        # database set up
        self.database.reset_db()
        self.session = self.database.get_session()

        # init data for tests
        self.main_user = add_user(
            self.session,
            CREATOR_USERNAME,
            CREATOR_EMAIL
        )

        self.second_user = add_user(
            self.session,
            COLLABORATOR_USERNAME,
            COLLABORATOR_EMAIL
        )



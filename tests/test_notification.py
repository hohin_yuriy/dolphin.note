import unittest

import dolphin_note_library.commands.notification as notification_commands
from dolphin_note_library.commands.note import add_note
from dolphin_note_library.commands.project import add_project
from tests.tests_set_up import (
    DolpinNoteTestCase,
    END_DATE,
    DELTA,
    NAME,
    DUE_DATE,
)


class NotificationTest(DolpinNoteTestCase):
    def setUp(self):
        super().setUp()
        self.project = add_project(
            self.session,
            self.main_user.id,
            NAME
        )

        self.note = add_note(
            self.session,
            self.main_user.id,
            name=NAME,
            project_id=self.project.id,
            due_date=DUE_DATE
        )
        self.notification = notification_commands.add_time_notification(
            self.session,
            self.main_user.id,
            self.note.id,
            END_DATE,
            DELTA
        )

    def test_time_notification(self):
        self.assertEqual(self.notification.note, self.note)
        self.assertEqual(self.notification.date, END_DATE)
        self.assertEqual(self.notification.delta, DELTA)

    def test_get_note_notifications(self):
        notifications = notification_commands.get_note_notifications(
            self.session,
            self.main_user.id,
            self.note.id
        )
        self.assertTrue(self.notification in notifications)

    def test_get_notification_by_id(self):
        notification = notification_commands.get_notification_by_id(
            self.session,
            self.main_user.id,
            self.notification.id,
        )
        self.assertEqual(notification, self.notification)

    def test_delete_time_notification(self):
        result = notification_commands.delete_time_notification(
            self.session,
            self.main_user.id,
            self.notification.id
        )
        self.assertTrue(result)

    def test_get_time_notifications(self):
        notifications = notification_commands.get_time_notifications(
            self.session,
            self.main_user.id,
            2*DELTA
        )
        self.assertTrue(self.notification in notifications)


def main():
    unittest.main()


if __name__ == '__main__':
    main()
import unittest

import dolphin_note_library.commands.note as note_commands
from dolphin_note_library.commands.project import add_project
from tests.tests_set_up import (
    DolpinNoteTestCase,
    NAME,
    DUE_DATE,
    COLLABORATOR_USERNAME,
)


class NoteTest(DolpinNoteTestCase):
    def setUp(self):
        super().setUp()
        self.project = add_project(
            self.session,
            self.main_user.id,
            NAME
        )

        self.note = note_commands.add_note(
            self.session,
            self.main_user.id,
            name=NAME,
            project_id=self.project.id,
            due_date=DUE_DATE
        )

    def test_note(self):
        self.assertEqual(self.note.name, NAME)
        self.assertEqual(self.note.project, self.project)
        self.assertEqual(self.note.due_date, DUE_DATE)

    def test_assign_user_to_note(self):
        self.note_collaborator = note_commands.assign_user_to_note(
            self.session,
            self.main_user.id,
            self.note.id,
            COLLABORATOR_USERNAME
        )
        self.assertEqual(self.note_collaborator.note_id, self.note.id)
        self.assertEqual(
            self.note_collaborator.collaborator_id,
            self.second_user.id
        )

    def test_get_all_project_root_nodes(self):
        notes = note_commands.get_all_project_root_notes(
            self.session,
            self.main_user.id,
            self.project.id
        )
        self.assertTrue(self.note in notes)

    def test_get_note_by_id(self):
        note = note_commands.get_note_by_id(
            self.session,
            self.main_user.id,
            self.note.id
        )
        self.assertEqual(note, self.note)

    def test_get_all_assigned_users_to_note(self):
        self.note_collaborator = note_commands.assign_user_to_note(
            self.session,
            self.main_user.id,
            self.note.id,
            COLLABORATOR_USERNAME
        )
        assigned_users = note_commands.get_note_assignees(
            self.session,
            self.main_user.id,
            self.note.id
        )
        self.assertTrue(self.note_collaborator in assigned_users)

    def test_get_all_project_notes(self):
        notes = note_commands.get_all_project_notes(
            self.session,
            self.main_user.id,
            self.project.id
        )
        self.assertTrue(self.note in notes)


def main():
    unittest.main()


if __name__ == '__main__':
    main()
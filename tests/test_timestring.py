import unittest
from datetime import (
    datetime,
    timedelta
)

from timestring import Date
from timestring import Range


class TimestringTests(unittest.TestCase):
    def test_fullstring(self):
        now = datetime.now()

        # DATE

        date = Date("may 23rd, 1988 at 6:24 am")
        self.assertEqual(date.year, 1988)
        self.assertEqual(date.month, 5)
        self.assertEqual(date.day, 23)
        self.assertEqual(date.hour, 6)
        self.assertEqual(date.minute, 24)


        # RANGE

        r = Range('From 04/17/13 04:18:00 to 05/01/13 17:01:00')
        self.assertEqual(r.start.year, 2013)
        self.assertEqual(r.start.month, 4)
        self.assertEqual(r.start.day, 17)
        self.assertEqual(r.start.hour, 4)
        self.assertEqual(r.start.minute, 18)
        self.assertEqual(r.end.year, 2013)
        self.assertEqual(r.end.month, 5)
        self.assertEqual(r.end.day, 1)
        self.assertEqual(r.end.hour, 17)
        self.assertEqual(r.end.minute, 1)

        _range = Range("between january 15th at 3 am and august 5th 5pm")
        self.assertEqual(_range[0].year, now.year)
        self.assertEqual(_range[0].month, 1)
        self.assertEqual(_range[0].day, 15)
        self.assertEqual(_range[0].hour, 3)
        self.assertEqual(_range[1].year, now.year)
        self.assertEqual(_range[1].month, 8)
        self.assertEqual(_range[1].day, 5)
        self.assertEqual(_range[1].hour, 17)

    def test_dates(self):
        date = Date("1-2-13 2 am")
        [self.assertEqual(*m) for m in
         ((date.year, 2013), (date.month, 1), (date.day, 2), (date.hour, 2), (date.minute, 0), (date.second, 0))]

        date = Date("dec 15th '01 at 6:25:01 am")
        [self.assertEqual(*m) for m in
         ((date.year, 2001), (date.month, 12), (date.day, 15), (date.hour, 6), (date.minute, 25), (date.second, 1))]

    def test_this(self):
        now = datetime.now()

        # this year
        year = Range('this year')
        self.assertEqual(year.start.year, now.year)
        self.assertEqual(year.start.month, 1)
        self.assertEqual(year.start.day, 1)
        self.assertEqual(year.start.hour, 0)
        self.assertEqual(year.start.minute, 0)
        self.assertEqual(year.end.year, now.year + 1)
        self.assertEqual(year.end.month, 1)
        self.assertEqual(year.end.day, 1)
        self.assertEqual(year.end.hour, 0)
        self.assertEqual(year.end.minute, 0)

        # 1 year (from now)

        year = Range('1 year')
        self.assertEqual(year.start.year, (now + timedelta(days=1)).year - 1)
        self.assertEqual(year.start.month, (now + timedelta(days=1)).month)
        self.assertEqual(year.start.day, (now + timedelta(days=1)).day)
        self.assertEqual(year.start.hour, 0)
        self.assertEqual(year.start.minute, 0)
        self.assertEqual(year.end.year, (now + timedelta(days=1)).year)
        self.assertEqual(year.end.month, (now + timedelta(days=1)).month)
        self.assertEqual(year.end.day, (now + timedelta(days=1)).day)
        self.assertEqual(year.end.hour, 0)
        self.assertEqual(year.end.minute, 0)

        #
        # this month
        #
        month = Range('this month')
        self.assertEqual(month.start.year, now.year)
        self.assertEqual(month.start.month, now.month)
        self.assertEqual(month.start.day, 1)
        self.assertEqual(month.start.hour, 0)
        self.assertEqual(month.start.minute, 0)
        self.assertEqual(month.end.year, month.start.year + (1 if month.start.month + 1 == 13 else 0))
        self.assertEqual(month.end.month, (month.start.month + 1) if month.start.month + 1 < 13 else 1)
        self.assertEqual(month.end.day, 1)
        self.assertEqual(month.end.hour, 0)
        self.assertEqual(month.end.minute, 0)

    def test_in(self):
        self.assertTrue(Date('yesterday') in Range("last 7 days"))
        self.assertTrue(Range('this month') in Range('this year'))
        self.assertTrue(Range('this day') in Range('this week'))

    def test_compare(self):
        self.assertFalse(Range('10 days') == Date('yestserday'))
        self.assertTrue(Date('yestserday') in Range('10 days'))
        self.assertTrue(Range('10 days') in Range('100 days'))

    def test_last(self):
        now = datetime.now()

        year = Range('last year')
        self.assertEqual(year.start.year, now.year - 1)
        self.assertEqual(year.start.month, now.month)
        self.assertEqual(year.start.day, now.day)
        self.assertEqual(year.start.hour, 0)
        self.assertEqual(year.start.minute, 0)
        self.assertEqual(year.end.year, now.year)
        self.assertEqual(year.end.month, now.month)
        self.assertEqual(year.end.day, now.day)
        self.assertEqual(year.end.hour, 0)
        self.assertEqual(year.end.minute, 0)
        self.assertTrue(Date('today') in year)


def main():
    unittest.main()


if __name__ == '__main__':
    main()
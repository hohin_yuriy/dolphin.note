***Dolphin.Note***

Dolphin note is a powerful task tracker for funny humans 
who have no a lot of time without planning


###   Introduction
This tracker consists of three libraries
* Core library that represents tracker logic and 
  provides API that helps to write your own interface to work with tracker
* CLI library that allows to work with tracker via console
* Django backend that allows to work with tracker via web


### Features
* Creating project (for example project "Film about the best girl in the world")
* we can add some label like "important" (this project really so important) 
* we can add some users to project that will take part in this project(frends of this girl)
* we can add some task(for example "Buy camera")
* and add user who will go for camera with you(task partisipant)
* and and some sub task(for example "Go to the shop")
* and add sub task(for example "Go away from house")
* and also you can add description about this task/project, set deadlines
* add some plan for task(dress socks(plan items - simple actions that don't have additional info, deadlines and so on))
* also you can add schedule, and some tasks will be added automaticly after some period of time
* also you can set a priority and status of the task


### Installing

Clonning project form bitbucket:
> git clone https://bitbucket.org/hohin_yuriy/dolphin.note

Install and update using pip:
> pip install -U dolphin.note

If you want to install only core library
```bash
cd core

pip install -U .

```

### Usage

```bash
dolph [ENTITY] [COMMAND] [ARGS]
```

Entitiess list:
* Project
* Note
* Notification
* Plan
* Label
* Schedule
* User
  
### A Simple examples

* Example Project usage

```bash
dolph project add --name "KS" --description "KS"
dolph project update --name "KSUSHA" --description "STERLIKOVA" --no-archive
dolph project show --id 1
dolph project remove --id 1
```


* Example Note usage

```bash
dolph note show --id 1
dolph note add --name "KS" --description "KS" --priority "Normal" --parent-note-id 1
dolph note update --id 1 --name "KSUSHA"

```



### Help
```bash
$ dolph [OPTIONS] COMMANDS --help
```

### Links

* yura.gogin@gmail.com - if you have quetions about tracker
* https://vk.com/id106663209 - Ksenia Sterlikova, the main inspirer of this tracker
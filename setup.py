import os

from setuptools import setup


def read(file_name):
    return open(os.path.join(os.path.dirname(__file__), file_name)).read()


setup(
    name="dolphin_note",
    version="2.0.1",
    author="Yuriy Hohin",
    author_email ="yura.gogin@gmail.com",
    description="A very usefull task tracker for Unicorns)))",
    license = "EULA",
    keywords ="task tracker note project",
    url="",
    packages=['dolphin_note_library', 'dolphin_note_cli', 'tests'],
    install_requires=["Click", "sqlalchemy", "datetime", 'timestring', 'six', 'django'],
    long_description=read('README.md'),
    entry_points='''
        [console_scripts]
        dolph=dolphin_note_cli.parser:entry_point
   
    ''',
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Issue Trackers",
        "License :: EULA",
    ],
)
from dateutil.relativedelta import relativedelta
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import (
    render,
    redirect,
)
from django.template.loader import render_to_string

import dolphin_note_library.commands.user as user_commands
from dolphin_note.views.forms.ouath import (
    SignUpForm,
    LoginForm,
)
from dolphin_note.views.utils.decorators import provide_session
from dolphin_note.views.utils.relativedelta_constants import WEEK
from dolphin_note_library.commands.label import find_labels_by_name_like
from dolphin_note_library.commands.note import find_notes_by_name_like
from dolphin_note_library.commands.notification import check_notifications
from dolphin_note_library.commands.project import find_projects_by_name_like


@provide_session
def signup(request, session):
    args = {'form': SignUpForm()}
    if request.POST:
        user_form = SignUpForm(request.POST)
        print(user_form)
        if user_form.is_valid():
            user_form.save()
            username = user_form.cleaned_data['username']
            password = user_form.cleaned_data['password1']
            user = auth.authenticate(username=username, password=password)
            alchemy_user = user_commands.add_user_with_id(session, user.id, user.username)
            auth.login(request, user)
            return redirect('/')

        args['form'] = user_form
        args['user'] = request.user

    return render(request, 'oauth/sign_up.html', args)


def login(request):
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = auth.authenticate(username=username, password=password)
            if user:
                auth.login(request, user)
                return redirect('/')

            login_error = 'Incorrect password.'
            args = {
                'form': form,
                'login_error': login_error
            }

            return render(request, 'oauth/login.html', args)

        login_error = 'Form is invalid.'
        args = {
            'form': form,
            'login_error': login_error
        }
        return render(request, 'oauth/login.html', args)

    form = LoginForm()
    return render(request, 'oauth/login.html', {'form': form})


def logout(request):
    auth.logout(request)
    return redirect('/')


@provide_session
def home(request, session):
    if request.user.is_authenticated:
        username = request.user.username
        due_date_notes, notifications = check_notifications(
            session,
            request.user.id,
            WEEK
        )
        notifications_list = render_to_string(
            'notifications_list.html',
            {
                'period': WEEK,
                'due_date_notes': due_date_notes,
                'notifications': notifications
            }
        )
        return render(
            request,
            'index.html',
            {
                'notifications_list': notifications_list,
                'username': username

            }
        )
    return render(request, 'oauth/not_auth.html')


@login_required
@provide_session
def search(request, session, search_value):

    projects = find_projects_by_name_like(session, request.user.id, text=search_value)
    notes = find_notes_by_name_like(session, request.user.id, text=search_value)
    labels = find_labels_by_name_like(session, request.user.id, text=search_value)

    return render(
        request,
        'search_list.html',
        {
            'projects': projects,
            'notes': notes,
            'labels': labels
        }
    )

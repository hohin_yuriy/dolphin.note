from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.urls import reverse

import dolphin_note_library.commands.label as label_commands
from dolphin_note.views.forms.label import (
    AddLabelForm,
    AssignLabelFormChoices,
)
from dolphin_note.views.utils.decorators import provide_session
from dolphin_note.views.utils.relativedelta_constants import WEEK
from dolphin_note.views.utils.template_rendering import (
    get_note_info_render,
    get_form_render,
    get_notifications_list)


@login_required
@provide_session
def labels_list(request, session):
    labels = label_commands.get_user_labels(session, request.user.id)
    if request.method == 'GET':
        return render(
            request,
            'labels_list.html',
            {
                'labels': labels
            }
        )


@login_required
@provide_session
def add_label(request, session):
    if request.method == 'POST':
        form = AddLabelForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            label = label_commands.add_label(session, request.user.id, name)
            return get_notifications_list(request, session, WEEK)
    else:
        form = AddLabelForm()
        return get_form_render(session, request, form, reverse('dolphin_note:add_label'))


@login_required
@provide_session
def assign_label_to_note(request, session, note_id):
    if request.method == 'POST':
        form = AssignLabelFormChoices(
            session,
            request.user.id,
            note_id,
            request.POST
        )
        if form.is_valid():
            name = form.cleaned_data['name']
            label = label_commands.get_label_by_name(session, request.user.id, name)
            note_label = label_commands.assign_label_to_note(
                session,
                request.user.id,
                note_id,
                label.id
            )
            return get_note_info_render(session, request, note_id)
    else:
        form = AssignLabelFormChoices(session, request.user.id, note_id)
        return get_form_render(
            session,
            request,
            form,
            reverse(
                'dolphin_note:assign_label',
                kwargs={
                    'note_id': note_id
                }
            )
        )


@login_required
@provide_session
def remove_note_label(request, session, note_id, label_id):
    if request.method == 'POST':
        result = label_commands.remove_note_label(
            session,
            request.user.id,
            note_id,
            label_id
        )
        return get_note_info_render(session, request, note_id)


@login_required
@provide_session
def remove_label(request, session, label_id):
    if request.method == 'POST':
        label = label_commands.get_label_by_id(session, request, label_id)
        session.delete(label)
        session.commit()
        labels = label_commands.get_user_labels(session, request.user.id)
        return render(
            request,
            'labels_list.html',
            {
                'labels': labels
            }
        )


@login_required
@provide_session
def show_label_notes(request, session, label_id):
    if request.method == 'GET':
        label = label_commands.get_label_by_id(session, request, label_id)
        return render(request, 'label_notes_list.html', {'label': label})
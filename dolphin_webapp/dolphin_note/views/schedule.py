from datetime import date

from django.shortcuts import render
from django.urls import reverse

import dolphin_note_library.commands.schedule as sheduler_commands
from dolphin_note.views.forms.schedule import AddScheduleForm
from dolphin_note.views.utils.decorators import provide_session
from dolphin_note.views.utils.template_rendering import get_form_render


def _show_schedulers(request, session):
    schedulers = sheduler_commands.get_all_user_schedules(session, request.user.id)
    sheduler_commands.create_user_notes_from_schedules(session, request.user.id)
    return render(
        request,
        'schedulers_list.html',
        {
            'schedules': schedulers
        }
    )


@provide_session
def show_schedules(request, session):
    if request.method == 'GET':
        return _show_schedulers(request, session)


@provide_session
def delete_scheduler(request, session, scheduler_id):
    if request.method == 'POST':
        result = sheduler_commands.remove_schedule(session, request.user.id, scheduler_id)
        if result:
            return _show_schedulers(request, session)


@provide_session
def add_recurring_note_creation(request, session, note_id=None):
    if request.method == 'POST':
        form = AddScheduleForm(request.POST)
        print(form.errors)
        if form.is_valid():
            delta = form.cleaned_data['delta']
            start_date = form.cleaned_data['start_date'].replace(tzinfo=None)
            end_date = form.cleaned_data['end_date']
            if end_date:
                end_date.replace(tzinfo=None)
            schedule = sheduler_commands.add_schedule(
                session,
                request.user.id,
                delta=delta,
                start_date=start_date,
                end_date=end_date,
                note_id=note_id
            )
            return _show_schedulers(request, session)

    form = AddScheduleForm(initial={
        'start_date': date.today(),
        'due_date': date.today()
    })
    return get_form_render(
        session,
        request,
        form,
        reverse(
            'dolphin_note:create_subnote_scheduler',
            kwargs={
                'note_id': note_id
            }
        )
    )






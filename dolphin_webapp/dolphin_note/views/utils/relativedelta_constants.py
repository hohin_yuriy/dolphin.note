"""This module represents """
from dateutil.relativedelta import relativedelta

DAY = relativedelta(days=1)
WEEK = relativedelta(weeks=1)
MONTH = relativedelta(months=1)
YEAR = relativedelta(years=1)
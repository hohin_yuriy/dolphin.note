from functools import wraps

from django.shortcuts import redirect

from dolphin_note_library.database import Database


def provide_session(func):
    """This decorator transmits session object to django view

    :param func: django view function
    """
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        session = Database().get_session()
        if not session:
            return redirect('/500/')
        return func(request, session, *args, **kwargs)

    return wrapper
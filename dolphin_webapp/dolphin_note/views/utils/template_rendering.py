"""
    This package provides rendering of
    most frequently used html templates
"""
from django.shortcuts import render

import dolphin_note_library.commands.label as label_commands
import dolphin_note_library.commands.note as note_commands
import dolphin_note_library.commands.project as project_commands
import dolphin_note_library.commands.notification as notification_commands


def get_note_info_render(session, request, note_id):
    """
    Renders note_info.html template and returns
    it as HttpResponce
    """
    note = note_commands.get_note_by_id(session, request.user.id, note_id)
    note_labels = label_commands.get_note_labels(session, request.user.id, note_id)
    notifications = notification_commands.get_note_notifications(session, request.user.id, note_id)
    return render(
        request,
        'note_info.html',
        {
            'note': note,
            'note_labels': note_labels,
            'notifications': notifications
        }
    )


def get_show_project_render(session, request, project_id):
    """
    Renders show_project.html template and returns
    it as HttpResponce
    """
    root_notes = note_commands.get_all_project_root_notes(session, request.user.id, project_id)
    project = project_commands.get_project_by_id(session, request.user.id, project_id)
    return render(
        request,
        'show_project.html',
        {
            'all_root_elems': root_notes,
            'project': project
        }
    )


def get_form_render(session, request, form, url):
    """
    Renders form_view.html template and returns
    it as HttpResponce
    """
    return render(
        request,
        'form_view.html',
        {
            'form': form,
            'url': url
        }
    )


def get_notifications_list(request, session, delta):
    """
    Renders notifications_list.html template and returns
    it as HttpResponce
    """
    due_date_notes, notifications = notification_commands.check_notifications(session, request.user.id, delta)
    return render(
        request,
        'notifications_list.html',
        {
            'due_date_notes': due_date_notes,
            'notifications': notifications,
            'period': delta
        }
    )
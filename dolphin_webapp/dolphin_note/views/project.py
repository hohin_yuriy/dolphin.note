from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import (
    render,
    redirect,
)
from django.urls import reverse

import dolphin_note_library.commands.project as project_commands
from dolphin_note.views.utils.decorators import provide_session
from dolphin_note.views.utils.template_rendering import (
    get_show_project_render,
    get_form_render,
)
from dolphin_note.views.forms.project import (
    ProjectCreateForm,
    AssignUserToProjectForm,
    ProjectUpdateForm,
)


@login_required
@provide_session
def create_project(request, session):
    if request.method == 'POST':
        form = ProjectCreateForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            project = project_commands.add_project(session, request.user.id, name)
            return render(
                request,
                'show_project.html',
                {
                    'project': project,
                    'all_root_elems': None
                }
            )
    else:
        form = ProjectCreateForm()
        return render(
            request,
            'form_view.html',
            {
                'form': form,
                'url': reverse('dolphin_note:create_project')
            }
        )


@login_required
@provide_session
def update_project(request, session, project_id):
    if request.method == 'POST':
        form = ProjectUpdateForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            is_archived = form.cleaned_data['is_archived']
            project = project_commands.update_project(session,
                                                      request.user.id,
                                                      project_id,
                                                      name,
                                                      is_archived)
            return get_show_project_render(session, request, project_id)
    else:
        project = project_commands.get_project_by_id(session, request.user.id, project_id)
        form = ProjectUpdateForm(initial={
            'name': project.name,
            'is_archived': project.is_archived
        })
        return get_form_render(
            session,
            request,
            form,
            reverse(
                'dolphin_note:update_project',
                kwargs={
                    'project_id': project_id
                }
            )
        )


@login_required
@provide_session
def project_list(request, session):
    if request.method == 'GET':
        current_projects = project_commands.get_all_user_projects(session, request.user.id, is_archived=False)
        archived_projects = project_commands.get_all_user_projects(session, request.user.id, is_archived=True)
        assigned_projects = project_commands.get_all_assigned_projects(session, request.user.id)

        return render(
            request,
            'projects_list.html',
            {
                'current_projects': current_projects,
                'archived_projects': archived_projects,
                'assigned_projects': assigned_projects
            }
        )


@login_required
@provide_session
def project_notes_tree(request, session, project_id):
    if request.method == 'GET':
        return get_show_project_render(session, request, project_id)


@provide_session
def delete_project(request, session, project_id):
    if request.method == 'POST':
        result = project_commands.remove_project(session, request.user.id, project_id)
        return HttpResponse("YES!")


@login_required
@provide_session
def assign_user_to_project(request, session, project_id):
    if request.method == 'POST':
        form = AssignUserToProjectForm(
            session,
            request.user.id,
            project_id,
            request.POST
        )
        if form.is_valid():
            username = form.cleaned_data['username']
            collaborator = project_commands.assign_user_to_project(session, request.user.id, project_id, username)
            return get_show_project_render(session, request, project_id)

    else:
        form = AssignUserToProjectForm(
            session,
            request.user.id,
            project_id
        )
        return get_form_render(
            session,
            request,
            form,
            reverse(
                'dolphin_note:assign_to_project',
                kwargs={
                    'project_id': project_id
                }
            )
        )


@login_required
@provide_session
def deassign_user_from_project(request, session, project_id, username):
    if request.method == 'POST':
        collaborator = project_commands.remove_project_assignee(session, request.user.id, project_id, username)
        if request.user.username == username:
            return redirect(reverse('dolphin_note:check_week_notifications'))
        return get_show_project_render(session, request, project_id)
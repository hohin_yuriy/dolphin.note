from datetime import date

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse

import dolphin_note_library.commands.note as note_commands
from dolphin_note.views.forms.note import (
    AddNoteForm,
    UpdateNoteForm,
    AssignUserToNoteForm,
)
from dolphin_note.views.utils.decorators import provide_session
from dolphin_note.views.utils.template_rendering import (
    get_show_project_render,
    get_form_render,
    get_note_info_render,
)
from dolphin_note_library.commands.user import get_user_by_username
from dolphin_note_library.utils.datetime import get_datetime_from_string


@login_required
@provide_session
def add_note(request, session, note_id=None, project_id=None):
    if request.method == 'POST':
        form = AddNoteForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            priority = form.cleaned_data['priority']
            due_date = form.cleaned_data['due_date']
            if due_date == '':
                due_date = None
            else:
                due_date = get_datetime_from_string(due_date)
            note = note_commands.add_note(session,
                                          request.user.id,
                                          name=name,
                                          project_id=project_id,
                                          priority=priority,
                                          due_date=due_date,
                                          parent_note_id=note_id)
            return get_show_project_render(session, request, note.project_id)

    form = AddNoteForm(initial={'due_date': date.today()})
    if note_id:
        return get_form_render(
            session,
            request,
            form,
            reverse(
                'dolphin_note:add_subnote',
                kwargs={
                    'note_id': note_id
                }
            )
        )
    else:
        return get_form_render(
            session,
            request,
            form,
            reverse(
                'dolphin_note:create_note',
                kwargs={
                    'project_id': project_id
                }
            )
        )


@login_required
@provide_session
def change_archive_status(request, session, note_id):
    if request.method == 'POST':
        note = note_commands.get_note_by_id(session, request.user.id, note_id)
        is_archived = not note.is_archived
        note = note_commands.update_note(session, request.user.id, note_id, archive=is_archived)
        return HttpResponse("Status changed!")


@login_required
@provide_session
def update_note(request, session, note_id):
    if request.method == 'POST':
        form = UpdateNoteForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            priority = form.cleaned_data['priority']
            due_date = form.cleaned_data['due_date']
            is_archived = form.cleaned_data['is_archived']
            if due_date == '':
                due_date = None
            else:
                due_date = get_datetime_from_string(due_date)

            note = note_commands.update_note(session,
                                             request.user.id,
                                             note_id,
                                             name=name,
                                             archive=is_archived,
                                             priority=priority,
                                             due_date=due_date)
            return get_show_project_render(session, request, note.project_id)
    else:
        note = note_commands.get_note_by_id(session, request.user.id, note_id)
        form = UpdateNoteForm(initial={
            'name': note.name,
            'priority': note.priority,
            'due_date': note.due_date,
            'is_archived': note.is_archived
        })
        return get_form_render(
            session,
            request,
            form,
            reverse(
                'dolphin_note:update_note',
                kwargs={
                    'note_id': note_id
                }
            )
        )


@login_required
@provide_session
def show_details(request, session, note_id):
    if request.method == 'GET':
        return get_note_info_render(session, request, note_id)


@login_required
@provide_session
def deassign_user_from_note(request, session, note_id, username):
    if request.method == 'POST':
        user = get_user_by_username(session, username)
        result = note_commands.remove_note_assignee(session, request.user.id, note_id, user.id)
        if result:
            return get_note_info_render(session, request, note_id)


@login_required
@provide_session
def assign_user_to_note(request, session, note_id):
    if request.method == 'POST':
        form = AssignUserToNoteForm(
            session,
            request.user.id,
            note_id,
            request.POST
        )
        if form.is_valid():
            username = form.cleaned_data['username']
            collaborator = note_commands.assign_user_to_note(session, request.user.id, note_id, username)
            if collaborator:
                return get_note_info_render(session, request, note_id)
    else:
        form = AssignUserToNoteForm(
            session,
            request.user.id,
            note_id
        )
        return get_form_render(
            session,
            request,
            form,
            reverse(
                'dolphin_note:assign_to_note',
                kwargs={
                    'note_id': note_id
                }
            )
        )


@login_required
@provide_session
def remove_note(request, session, note_id):
    if request.method == 'POST':
        note = note_commands.get_note_by_id(session, request.user.id, note_id)
        project = note.project
        note_commands.remove_note(session, request.user.id, note_id)
        return get_show_project_render(session, request, project.id)


@login_required
@provide_session
def show_assigned_notes(request, session):
    if request.method == 'GET':
        assigned_notes = note_commands.get_all_assigned_notes_to_user(session, request.user.id)
        return render(
            request,
            'assigned_to_me_notes_list.html',
            {
                'assigned_notes': assigned_notes
            }
        )
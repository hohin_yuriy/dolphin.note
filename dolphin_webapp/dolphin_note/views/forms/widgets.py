import re

from dateutil.relativedelta import relativedelta
from django.forms import (
    CharField,
    TextInput,
)


def validate_timedelta_string(value):
    """ Validates timedelta string correctness.
        Returns: _sre.SRE_Match object
    """
    return re.match(
        (
            r'('
            r'(\d+)(\s)*'
            r'(years|months|weeks|days|hours|minutes|seconds)'
            r'(,)?'
            r'(\s*)'
            r')+'
        ),
        value
    )


def get_relative_delta_from_string(value):
    """ Transorms string into relativedelta object.
        Returns: relativedelta object
    """
    return relativedelta(
        **{
            key: int(value)
            for value, key
            in re.findall(
            (
                r'(\d+)\s*'
                r'(years|months|weeks|days|hours|minutes|seconds)'
            ),
            value,
        )
        }
    )


class TimeDeltaField(CharField):
    """
    This field parse string to relativedelta object
    """
    widget = TextInput(
        attrs={
            'placeholder': 'example: 5 years, 5 days'
        }
    )

    def to_python(self, value):
        """Return a relativedelta."""
        if value in self.empty_values:
            return None
        if validate_timedelta_string(value):
            try:
                return get_relative_delta_from_string(value)
            except Exception:
                pass

        raise ValueError('Invalid delta format')



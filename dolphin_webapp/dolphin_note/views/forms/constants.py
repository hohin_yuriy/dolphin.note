from dolphin_note_library.models.note import NotePriority

PRIORITY_CHOICES = (
    (NotePriority.LOW.value, 'Low'),
    (NotePriority.NORMAL.value, 'Normal'),
    (NotePriority.IMPORTANT.value, 'Important'),
    (NotePriority.HIGH.value, 'High'),
)
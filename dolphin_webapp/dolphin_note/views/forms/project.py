from django.forms import (
    Form,
    CharField,
    TextInput,
    BooleanField,
    CheckboxInput,
    ChoiceField,
)

from dolphin_note_library.commands.project import get_all_project_assignees
from dolphin_note_library.commands.user import get_all_users


class ProjectCreateForm(Form):
    name = CharField(
        widget=TextInput(
            attrs={
                'class': 'input',
                'placeholder': 'Enter note name'
            }
        ),
        required=True
    )


class ProjectUpdateForm(ProjectCreateForm):
    is_archived = BooleanField(
        widget=CheckboxInput(
            attrs={
                'class': 'required checkbox form-control',
                'placeholder': 'Archive'
            }
        ),
        required=False
    )


class AssignUserToProjectForm(Form):
    def __init__(self, session, user_id, project_id, *args, **kwargs):
        super(AssignUserToProjectForm, self).__init__(*args, **kwargs)
        users = get_all_users(session)
        project_assignees = get_all_project_assignees(
            session,
            user_id,
            project_id
        )
        assigned_users_ids = [
            assigned_user.member
            for assigned_user in project_assignees
        ]
        users_list_choices = [
            (user.username, user.username)
            for user in users
            if (user.id != user_id) and (user not in assigned_users_ids)
        ]
        self.fields['username'] = ChoiceField(choices=users_list_choices)

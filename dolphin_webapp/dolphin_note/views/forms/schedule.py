from datetime import datetime

from django import forms

from dolphin_note.views.forms.widgets import TimeDeltaField


class AddScheduleForm(forms.Form):
    delta = TimeDeltaField(label='Delta')
    start_date = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={
                'type': "datetime-local",
                'min': datetime.today().strftime('%Y-%m-%dT%H:%M')
            },
        ),
        input_formats=['%Y-%m-%dT%H:%M'],
        required=True
    )
    end_date = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={
                'type': "datetime-local",
                'min': datetime.today().strftime('%Y-%m-%dT%H:%M')
            }
        ),
        input_formats=['%Y-%m-%dT%H:%M'],
        required=False
    )
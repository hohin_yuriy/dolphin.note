from datetime import date

from django import forms
from django.forms import (
    BooleanField,
    CheckboxInput,
    ChoiceField,
)

from dolphin_note_library.commands.note import get_note_assignees
from dolphin_note_library.commands.user import get_all_users
from .constants import PRIORITY_CHOICES


class AddNoteForm(forms.Form):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'input',
                'placeholder': 'Enter note name'
            }
        ),
        required=True
    )
    priority = forms.ChoiceField(
        choices=PRIORITY_CHOICES,
        required=False,
        widget=forms.Select()
    )

    due_date = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'type': "date",
                'min': date.today()
            }
        ),
        required=False
    )

    def __init__(self, *args, **kwargs):
        super(AddNoteForm, self).__init__(*args, **kwargs)


class UpdateNoteForm(AddNoteForm):
    is_archived = BooleanField(
        widget=CheckboxInput(
            attrs={
                'class': 'required checkbox form-control',
                'placeholder': 'Archive'
            }
        ),
        required=False
    )


class AssignUserToNoteForm(forms.Form):
    def __init__(self, session, user_id, note_id, *args, **kwargs):
        super(AssignUserToNoteForm, self).__init__(*args, **kwargs)
        users = get_all_users(session)
        project_assignee = get_note_assignees(session, user_id, note_id)
        assigned_users_ids = [
            assigned_user.collaborator
            for assigned_user in project_assignee
        ]
        print(assigned_users_ids)
        users_list_choices = [
            (user.username, user.username)
            for user in users
            if (user.id != user_id) and (user not in assigned_users_ids)
        ]
        self.fields['username'] = ChoiceField(choices=users_list_choices)



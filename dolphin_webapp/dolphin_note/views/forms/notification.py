from datetime import date

from django import forms
from django.forms import DateField, DateInput

from dolphin_note.views.forms.widgets import TimeDeltaField


class NotificationCheckPeriodForm(forms.Form):
    delta = TimeDeltaField(label='Delta', required=False)


class AddNotificationForm(forms.Form):
    delta = TimeDeltaField(label='Delta', required=False)
    notification_date = DateField(
        widget=DateInput(
            attrs={
                'type': "date",
                'min': date.today()
            }
        ),
        required=False
    )

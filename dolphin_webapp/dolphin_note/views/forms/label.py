from django import forms
from django.forms import ChoiceField

from dolphin_note_library.commands.label import (
    get_note_labels,
    get_user_labels,
)


class AddLabelForm(forms.Form):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'input',
                'placeholder': 'Enter note name'
            }
        ),
        required=True
    )


class AssignLabelForm(forms.Form):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'input',
                'placeholder': 'Enter note name'
            }
        ),
        required=True
    )


class AssignLabelFormChoices(AssignLabelForm):
    def __init__(self, session, user_id, note_id, *args, **kwargs):
        super(AssignLabelFormChoices, self).__init__(*args, **kwargs)
        labels = get_user_labels(session, user_id)
        note_labels_ids = [
            note_label.label_id
            for note_label in get_note_labels(session, user_id, note_id)
        ]
        label_list_choices = [
            (label.name, label.name)
            for label in get_user_labels(session, user_id)
            if label.id not in note_labels_ids
        ]
        self.fields['name'] = ChoiceField(choices=label_list_choices)
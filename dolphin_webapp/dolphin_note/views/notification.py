from datetime import datetime, date

from dateutil.relativedelta import relativedelta
from django.urls import reverse

import dolphin_note_library.commands.notification as notification_commands
from dolphin_note.views.forms.notification import (
    AddNotificationForm,
    NotificationCheckPeriodForm,
)
from dolphin_note.views.utils.decorators import provide_session
from dolphin_note.views.utils.relativedelta_constants import (
    WEEK,
    DAY,
)
from dolphin_note.views.utils.template_rendering import (
    get_form_render,
    get_note_info_render,
)
from dolphin_note.views.utils.template_rendering import get_notifications_list


@provide_session
def add_notification(request, session, note_id):
    if request.method == 'POST':
        form = AddNotificationForm(request.POST)
        if form.is_valid():
            delta = form.cleaned_data['delta']
            if not delta:
                delta = relativedelta()
            notification_date = form.cleaned_data['notification_date']
            notification_date = datetime(notification_date.year,
                                         notification_date.month,
                                         notification_date.day)
            notification = notification_commands.add_time_notification(session, request.user.id,
                                                                       note_id, notification_date, delta)
            return get_note_info_render(session, request, note_id)

    form = AddNotificationForm(
        initial={
            'notification_date': date.today()
        }
    )
    return get_form_render(
        session,
        request,
        form,
        reverse(
            'dolphin_note:add_notification',
            kwargs={
                'note_id': note_id
            }
        )
    )


@provide_session
def remove_notification(request, session, notification_id):
    if request.method == 'POST':
        note_id = notification_commands.get_notification_by_id(
            session,
            request.user.id,
            notification_id
        ).note_id
        result = notification_commands.delete_time_notification(
            session,
            request.user.id,
            notification_id
        )
        if result:
            return get_note_info_render(session, request, note_id)


@provide_session
def check_today_notifications(request, session):
    if request.method == 'GET':
        return get_notifications_list(
            request,
            session,
            DAY
        )


@provide_session
def check_week_notifications(request, session):
    if request.method == 'GET':
        return get_notifications_list(
            request,
            session,
            WEEK
        )


@provide_session
def check_custom_period_notifications(request, session):
    if request.method == 'GET':
        form = NotificationCheckPeriodForm()
        return get_form_render(
            session,
            request,
            form,
            reverse('dolphin_note:check_custom_period_notifications')
        )
    else:
        form = NotificationCheckPeriodForm(request.POST)
        if form.is_valid():
            delta = form.cleaned_data['delta']
            return get_notifications_list(request, session, delta)
from django.conf.urls import url, include

from dolphin_note.views import (
    common,
    label,
    project,
    note,
    notification,
    schedule
)

app_name = "dolphin_note"

sheduler_patterns = [
    url(r'(?P<scheduler_id>[0-9]+)/delete/$', schedule.delete_scheduler, name='delete_schedule'),
    url(r'^$', schedule.show_schedules, name='show_schedules')
]

#####

exact_notification_patterns = [
    url(r'^remove_notification/$', notification.remove_notification, name='delete_notification'),
]

notifications_patterns = [
    url(r'^(?P<notification_id>[0-9]+)/', include(exact_notification_patterns)),
    url(r'today/$', notification.check_today_notifications, name='check_today_notifications'),
    url(r'week/$', notification.check_week_notifications, name='check_week_notifications'),
    url(r'custom/$', notification.check_custom_period_notifications, name='check_custom_period_notifications'),
]


#####

exact_label_patterns = [
    url(r'^remove/$', label.remove_label, name='delete_label'),
    url(r'(?P<note_id>[0-9]+)/delete_label/$', label.remove_note_label, name='deassign_label'),
    url(r'(?P<note_id>[0-9]+)/delete_label/$', label.remove_note_label, name='deassign_from'),
    url(r'show_notes/$', label.show_label_notes, name='show_label_notes')
]

label_patterns = [
    url(r'^(?P<label_id>[0-9]+)/', include(exact_label_patterns)),
    url(r'^create/$', label.add_label, name='add_label'),
]


#####

exact_note_patterns = [
    url(r'^assign/$', note.assign_user_to_note, name='assign_to_note'),
    url(r'^deassign/(?P<username>.+)/$', note.deassign_user_from_note, name='deassign_from_note'),
    url(r'^update/$', note.update_note, name='update_note'),
    url(r'^update_archive_status/$', note.change_archive_status, name='update_archive_status'),
    url(r'^remove/$', note.remove_note, name='delete_note'),
    url(r'^show/', note.show_details, name='show_note'),
    url(r'create/', note.add_note, name='add_subnote'),
    url(r'add_notification/$', notification.add_notification, name='add_notification'),
    url(r'add_label/$', label.assign_label_to_note, name='assign_label'),
    url(r'create_subnote_scheduler/$', schedule.add_recurring_note_creation, name='create_subnote_scheduler')
]

notes_patterns = [
    url(r'^(?P<note_id>[0-9]+)/', include(exact_note_patterns)),
]


#####

exact_project_patterns = [
    url(r'^assign/$', project.assign_user_to_project, name='assign_to_project'),
    url(r'^deassign/(?P<username>.+)/$', project.deassign_user_from_project, name='deassign_from_project'),
    url(r'^update/$', project.update_project, name='update_project'),
    url(r'^remove/$', project.delete_project, name='delete_project'),
    url(r'^show_notes/', project.project_notes_tree, name='show_notes'),
    url(r'^create/$', note.add_note, name='create_note'),
    url(r'^create_scheduler/$', schedule.add_recurring_note_creation, name='create_sheduler'),
]

projects_patterns = [
    url(r'^(?P<project_id>[0-9]+)/', include(exact_project_patterns)),
    url(r'^create/$', project.create_project, name='create_project'),
]


search_patterns = [
    url(r'^(?P<search_value>[a-zA-Z0-9]+)/', common.search),
]


#####

oauth_patterns = [
    url(r'^login/$', common.login, name='login'),
    url(r'^logout/$', common.logout, name='logout'),
    url(r'^signup/$', common.signup, name='signup'),
]

urlpatterns = [
    url(r'^$', common.home, name='home'),
    url(r'^', include(oauth_patterns)),
    url(r'^project/', include(projects_patterns)),
    url(r'^projects_list/', project.project_list, name='projects_list'),
    url(r'^label/', include(label_patterns)),
    url(r'^labels_list/$', label.labels_list, name='labels_list'),
    url(r'^note/', include(notes_patterns)),
    url(r'^search/', include(search_patterns)),
    url(r'^assigned_notes/', note.show_assigned_notes, name='show_assigned_notes'),
    url(r'^notifications/', include(notifications_patterns)),
    url(r'^scheduler/', include(sheduler_patterns)),
]
from django import template
from django.utils.timesince import timesince
from datetime import datetime

register = template.Library()


def timedelta(value, arg=None):
    if not value:
        return ''
    else:
        now = datetime.now()
        cmp = datetime.now() + value
        return timesince(now, cmp)


register.filter('timedelta', timedelta)
from django.apps import AppConfig


class DolphinNoteConfig(AppConfig):
    name = 'dolphin_note'

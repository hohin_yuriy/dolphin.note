$(document).on('keyup', '#livesearch', function (e) {
    var searchValue = document.getElementById("livesearch").value;
    showResult(searchValue);
    }
)

function showResult(str) {
  if (str.length==0) {
    document.getElementById("livesearch").innerHTML="";
    document.getElementById("livesearch").style.border="0px";
    return;
  }

  xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("appContent").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","search/"+str+"/", true);
  xmlhttp.send();
}
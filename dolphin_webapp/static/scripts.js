$(document).on('click', '.post-action', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var sendingUrl = this.href;
    csrf = jQuery("[name=csrfmiddlewaretoken]").val();
    const xhr = new XMLHttpRequest();
    xhr.open('POST', sendingUrl, true);
    xhr.setRequestHeader("X-CSRFToken", csrf);
    xhr.onreadystatechange = function() {
    if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
        $("#appContent").html(xhr.responseText);
    }
}
    xhr.send();
});

updateProjects();
updateLabels();

$(document).on('click', '.left-pannel-post', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var sendingUrl = this.href;
    csrf = jQuery("[name=csrfmiddlewaretoken]").val();
    var xhr = new XMLHttpRequest();
    console.log(sendingUrl);
    xhr.open('POST', sendingUrl, true);
    xhr.setRequestHeader("X-CSRFToken", csrf);
    xhr.send();
});

$(document).on('click', '.get-action', function (e) {
    e.preventDefault();
    e.stopPropagation();
    let sendingUrl = this.href;
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
    if (xhr.status === 200) {
        $("#appContent").html(xhr.responseText)
    }
    else {
        alert('Request failed.  Returned status of ' + xhr.status);
    }
    };
    xhr.open('GET', sendingUrl);
    xhr.send()
});

function updateProjects() {
    setTimeout(function () {
        var sendingUrl = "projects_list/";
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
        if (xhr.status === 200) {
            $("#projectsList").html(xhr.responseText)
         }
        };
    xhr.open('GET', sendingUrl, true);
    xhr.send()
    }, 100)
}
function updateLabels() {
    setTimeout(function () {
        var sendingUrl = "labels_list/";
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
        if (xhr.status === 200) {
            $("#labelsList").html(xhr.responseText)
         }
        };
    xhr.open('GET', sendingUrl, true);
    xhr.send()
    }, 100)
}
$(document).on('click', '.update-project-list', function (e) {
    e.preventDefault();
    updateProjects()
});
$(document).on('click', '.update-labels-list', function (e) {
    e.preventDefault();
    updateLabels()
});

$(document).on('click', '.strike-out', function (e) {
    e.preventDefault();
    if (e.target.matches('a.strike-out')) {
        e.stopImmediatePropagation();
        let sendingUrl = e.target.href;
        csrf = jQuery("[name=csrfmiddlewaretoken]").val();
        const xhr = new XMLHttpRequest();
        xhr.open('POST', sendingUrl, true);
        xhr.setRequestHeader("X-CSRFToken", csrf);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                var elements = e.target.parentNode.parentNode.querySelectorAll('.strike-out');
                if ($(e.target).css("text-decoration").includes('line-through')) {
                    for (var i = 0; i < elements.length; i++) {
                        $(elements[i]).css("text-decoration", "")
                    }
                } else {
                    for (var i = 0; i < elements.length; i++) {
                        $(elements[i]).css("text-decoration", "line-through")
                    }
                }
            }
        };
        xhr.send()
    }
});
$(document).on("submit", "form", function (event){
    event.preventDefault();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', $(this).attr('action'), true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function() {
        if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
        $("#appContent").html(xhr.responseText);
        updateProjects();
        updateLabels();
        }
    };
    xhr.send($(this).serialize());
});


$(document).on('click', 'a', function() {
   $('a').removeClass('active');
   $(this).addClass('active');
});



"""
Utils folder consists of:
    - logging.py:
      consists of
      * catch_exceptions_and_log decorator that provides
      logging and catching exceptions in commands
      Use:
      >>> @provide_logging

      * configure_logging(logger_name, level, log_directory, log_format, log_filename)
        Allows to set logger configuration to the logger with logger_name


    - exceptions.py:
    consists of custom Exceptions for tracker library
    BadUsageException - provides raising exceprions
    linked with incorrect data input
    Use:
    >>> raise BadUsageException(TEXT)

    - datetime.py
    consists of some usefull functions
    to easy work with datetime objects

    * get_datetime_from_string:
    Use timestring library to transer
    string with datetime in a human format
    to datetime object

    * DeltaType
    enum, use to set time period in weeks, days, months
    with get_relative_delta

    * get_relative_delta(delta_type, quantity)
    transfers DeltaType enum and quantity to
    relativedelta object
"""
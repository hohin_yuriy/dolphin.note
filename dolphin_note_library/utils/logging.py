"""
This module consists tool for initializing
logger for dolphin_note_library library
for better programming experience on Python

(c) Hohin Yuri, 2018
"""

import logging
from datetime import datetime

import os
from functools import wraps

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import NoResultFound

from dolphin_note_library.config import (
    LOGGER_NAME,
    LOG_LEVEL,
    LOG_DIRECTORY,
    LOG_FORMAT,
)
from dolphin_note_library.utils.exceptions import BadUsageException


def get_logger(logger_name):
    return logging.getLogger(logger_name)


def configure_logging(
        logger_name,
        level,
        log_directory,
        log_format,
        log_filename=None
):
    """
    Configure logging to log to log file
    """
    log_level_dict = {
        "DISABLED": logging.CRITICAL,
        "INFO": logging.INFO,
        "ERROR": logging.ERROR
    }
    level = log_level_dict[level]
    if not log_filename:
        log_filename = datetime.now().strftime('%Y-%m-%d') + '.log'

    logger = get_logger(logger_name)
    logger.setLevel(level)

    formatter = logging.Formatter(log_format)

    if not os.path.exists(log_directory):
        os.makedirs(log_directory, exist_ok=True)
    if not os.path.exists(log_directory + log_filename):
        open(log_directory + "/" + log_filename, "w")

    fh = logging.FileHandler(filename=log_directory + "/" + log_filename)
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)


def provide_logging(func):
    """
    This decorator provides logging activities for calling functions
    :param func: function that we want to log
    """
    configure_logging(logger_name=LOGGER_NAME, level=LOG_LEVEL,
                      log_directory=LOG_DIRECTORY, log_format=LOG_FORMAT)
    logger = get_logger(LOGGER_NAME)

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            logger.info("Called func: {}".format(func.__name__))
            logger.debug("Args: {0}, \n\tKwargs: {1}".format(args, kwargs))
            result = func(*args, **kwargs)
            logger.debug("{0} returned: {1}".format(func.__name__, result))
            return result
        except NoResultFound as e:
            logger.error(str(e))
            raise
        except SQLAlchemyError as e:
            logger.error(str(e))
            raise
        except BadUsageException as e:
            logger.error(str(e))
            raise
        except PermissionError as e:
            logger.error(str(e))
            raise
        except Exception as e:
            logger.error(str(e))
            raise

    return wrapper

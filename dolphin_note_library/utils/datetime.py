import enum
from datetime import datetime

import timestring
from dateutil import relativedelta


def get_datetime_from_string(string_date):
    """
    Return datetime object from date specified in free format
    :param string_date: string with date
    :return: date time
    """
    date = timestring.Date(string_date)
    date = datetime(
        year=date.year,
        month=date.month,
        day=date.day,
        hour=date.hour,
        minute=date.minute,
        second=date.second
    )
    return date


def get_relative_delta(delta_type, quantity):
    """
    Gets relativedelta object from
    delta_type types(DAYS, MONTHS, WEEKS, YEARS)
    :param period_type: DeltaType enum
    :param periods_number: int, number of DAYS, MONTHS, WEEKS, YEARS
    :return:
    """
    if delta_type == DeltaType.DAYS.value:
        return relativedelta.relativedelta(days=quantity)
    elif delta_type == DeltaType.WEEKS.value:
        return relativedelta.relativedelta(months=quantity)
    elif delta_type == DeltaType.MONTHS.value:
        return relativedelta.relativedelta(months=quantity)
    elif delta_type == DeltaType.YEARS.value:
        return relativedelta.relativedelta(years=quantity)
    elif delta_type == DeltaType.HOURS.value:
        return relativedelta.relativedelta(hours=quantity)


class DeltaType(enum.Enum):
    """Class represents differend types of time periods"""
    DAYS = "DAYS"
    WEEKS = "WEEKS"
    MONTHS = "MONTHS"
    YEARS = "YEARS"

"""
This module cosists of all exceptions
that I use in this project
"""


class BadUsageException(Exception):
    """This error occurs when we use our tracker incorrectly"""
    pass


class DBException(Exception):
    """Exception that occurs when SQLAlchemy can't create DB"""
    pass

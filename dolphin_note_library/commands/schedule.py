"""
    This code provides operations with Scheduler and NoteTemplate entity
"""
from datetime import datetime, date

from sqlalchemy.sql.elements import and_

from dolphin_note_library.commands.note import (
    add_note
)
from dolphin_note_library.models.schedule import (
    Schedule,
)
from dolphin_note_library.utils.logging import provide_logging


@provide_logging
def add_schedule(session, user_id, delta, start_date, end_date, note_id):
    """
    Creates Schedule object in db

    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param delta: timedelta, repeating period
    :param start_date: start of creating notes
    :param end_date: end of creating notes
    """
    schedule = Schedule(
        creator_id=user_id,
        delta=delta,
        start_date=start_date,
        end_date=end_date,
        last_created=start_date,
        note_id=note_id
    )
    session.add(schedule)
    session.commit()
    print("3")
    return schedule


@provide_logging
def add_note_copy(session, user_id, note):
    """
    Creates note from template

    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_template: note template object
    """
    note = add_note(
        session,
        user_id,
        name=note.name,
        project_id=note.project_id,
        parent_note_id=note.parent_note_id,
        priority=note.priority,
        due_date=note.due_date
    )
    return note


@provide_logging
def get_schedule_by_id(session, user_id, shaduler_id):
    """
    Finds project with id

    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the sheduler where we provides this operation
    :return: project object
    """
    project = session.query(Schedule).filter(Schedule.id == shaduler_id).one()
    return project


@provide_logging
def get_all_user_schedules(session, user_id):
    """
    Finds all prrojects where user assigned

    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :return: projects list
    """
    shadulers = session.query(Schedule).filter(Schedule.creator_id == user_id).all()
    return shadulers


@provide_logging
def remove_schedule(session, user_id, schedule_id):
    """
    Removes schedule from db

    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param schedule_id: id of the schedule where we provides this operation
    :return: True or exception
    """
    schedule = get_schedule_by_id(session, user_id, schedule_id)
    session.delete(schedule)

    session.commit()
    return True


@provide_logging
def delete_all_expired_schedules(session, user_id):
    """
    Deletes all expired schedules

    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :return: True or excception
    """
    current_date = datetime.combine(date.today(), datetime.min.time())
    expired_schedules = session.query(Schedule).filter(and_(Schedule.end_date < current_date,
                                                            Schedule.creator_id == user_id)).all()
    for schedule in expired_schedules:
        session.delete(schedule)
    session.commit()
    return True


@provide_logging
def create_user_notes_from_schedules(session, user_id):
    """
    Adds notes from schedule of the user

    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :return: note objects list
    """
    delete_all_expired_schedules(session, user_id)

    current_date = datetime.today()
    user_schedulers = get_all_user_schedules(session, user_id)
    notes = []
    for schedule in user_schedulers:
        if schedule.last_created == current_date == schedule.start_date:
            note = add_note_copy(session, user_id, schedule.note)
            schedule.last_created = schedule.last_created + schedule.delta

        delta = schedule.delta

        while (schedule.last_created <= current_date - delta and
              (schedule.end_date is None or schedule.last_created <= schedule.end_date - delta)):

            planned_note = add_note_copy(session, user_id, schedule.note)
            notes.append(planned_note)
            schedule.last_created += delta
        session.add(schedule)
        session.commit()

    return notes
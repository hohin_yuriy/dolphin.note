"""
    This code provides operations with Project entity
"""

from dolphin_note_library.models.user import User
from dolphin_note_library.utils.logging import provide_logging


@provide_logging
def get_user_by_username(session, username):
    """
    Finds user by its username
    :param session: object that establishes all conversations with the database
    :param username: name of the user
    :return: User object
    """
    user = session.query(User).filter(User.username == username).one()
    return user


@provide_logging
def get_user_by_id(session, user_id):
    """
    Finds user by its id
    :param session: object that establishes all conversations with the database
    :param user_id: id of User object
    :return: User object oe exception
    """
    user = session.query(User).filter(User.id == user_id).one()
    return user


@provide_logging
def get_all_users(session):
    """Finds all users in tracker

    :param session: object that establishes all conversations with the database
    :return: list of User objects
    """
    users = session.query(User).all()
    return users


@provide_logging
def add_user(session, username, email):
    """
    Adds User to Tracker
    :param session: object that establishes all conversations with the database
    :param username: uncial name of the user
    :param email: users email
    :return: User object or None
    """
    user = User(username=username, email=email)
    session.add(user)
    session.commit()
    return user


@provide_logging
def add_user_with_id(session, id, username, email=None):
    """
    This method for adding user with with some web framework
    :param session: object that establishes all conversations with the database
    :param username: uncial name of the user
    :param email: users email
    :return: User object or None
    """
    user = User(id=id, username=username, email=email)
    session.add(user)
    session.commit()
    return user

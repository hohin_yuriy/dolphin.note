"""
    This code provides operations with Time Notification entity
"""

from datetime import (
    timedelta,
    date,
)

from sqlalchemy import and_

from dolphin_note_library.models.note import Note
from dolphin_note_library.models.notification import TimeNotification
from dolphin_note_library.utils.logging import provide_logging


@provide_logging
def add_time_notification(session, user_id, note_id, date, delta):
    """
    Adds time notification
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param date: date when notification will be showed
    :param recurring_type: type of reccuring notifications updating
    :return: TimeNotification object
    """
    time_notification = TimeNotification(
        note_id=note_id,
        date=date,
        delta=delta,
        creator_id=user_id
    )
    session.add(time_notification)
    session.commit()
    return time_notification


@provide_logging
def get_note_notifications(session, user_id, note_id):
    """
    Gets all notifications of the note
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :return: TimeNotification object
    """
    update_time_notifications(session, user_id)
    notifications = (session.query(TimeNotification)
                            .filter(and_(TimeNotification.note_id == note_id,
                                         TimeNotification.date >= date.today())).all())
    return notifications


@provide_logging
def get_notification_by_id(session, user_id, reminder_id):
    """
    Finds time notification with id
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param reminder_id: id of the time notification
    :return: TimeNotification object
    """
    reminder = session.query(TimeNotification).filter(TimeNotification.id == reminder_id).one()
    return reminder


@provide_logging
def delete_time_notification(session, user_id, reminder_id):
    """
    Deletes time notification
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param reminder_id: deletes time notification from db
    :return: True or exception
    """
    reminder = get_notification_by_id(session, user_id, reminder_id)
    session.delete(reminder)
    session.commit()
    return reminder


@provide_logging
def get_expired_notes(session, user_id):
    """
    Return all due dates before today
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :return: list of Note objects
    """
    expired_notes = session.query(Note).filter(and_(Note.due_date < date.today(),
                                                    Note.is_archived == False)).all()
    return expired_notes


@provide_logging
def get_expired_notifications(session, user_id):
    """
    Return all due dates before today
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :return: list of Note objects
    """
    all_expired_notifications = (session.query(TimeNotification)
                                       .filter(TimeNotification.date < date.today()).all())
    # notifications in non archived notes
    expired_notifications = [expired_notification
                             for expired_notification in all_expired_notifications
                             if not expired_notification.note.is_archived]
    return expired_notifications


@provide_logging
def get_due_date_notes(session, user_id, period=timedelta(days=0)):
    """
    Gets all notes that deadline expires for period after today
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param period:  period of notifications checking
    :return: list of Note objects
    """
    today = date.today()
    due_date_notes = session.query(Note).filter(and_(Note.creator_id == user_id,
                                                     Note.due_date >= today,
                                                     Note.due_date <= today + period)).all()
    return due_date_notes


@provide_logging
def get_time_notifications(session, user_id, period=timedelta(days=0)):
    """
    Gets all notifications for period after today
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param period: period of notifications checking
    :return: Notes list
    """
    today = date.today()
    notifications = (session.query(TimeNotification)
                            .filter(and_(TimeNotification.creator_id == user_id,
                                         TimeNotification.date >= today,
                                         TimeNotification.date <= date.today() + period)).all())
    return notifications


@provide_logging
def update_time_notification(session, user_id, notification):
    """
    Updates notification if it has a reccuring type
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param notification: notification object where we updating due date
    :return: note object
    """
    if not notification.delta:
        session.delete(notification)
    else:
        delta = notification.delta
        while notification.date < date.today():
            notification.date += delta
        session.add(notification)
    session.commit()


@provide_logging
def update_time_notifications(session, user_id):
    expired_notifications = get_expired_notifications(session, user_id)
    for expired_notification in expired_notifications:
        update_time_notification(session, user_id, expired_notification)


@provide_logging
def check_notifications(session, user_id, period=timedelta(days=0)):
    """
    Checks notifications and deadlines in some period and updates expired
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param period: period of notifications checking
    :return: Notes list, TimeNotifications list
    """

    due_date_notes = get_due_date_notes(session, user_id, period)
    notifications = get_time_notifications(session, user_id, period)

    update_time_notifications(session, user_id)

    return due_date_notes, notifications
"""
    This code provides operations with Label entity
"""

from sqlalchemy import and_

from dolphin_note_library.config import SEARCH_LIMIT
from dolphin_note_library.models.label import (
    Label,
    NoteLabel,
)
from dolphin_note_library.utils.logging import provide_logging


@provide_logging
def add_label(session, user_id, label_name):
    """
    Adds Label object to DB
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param label_name: name of the adding label
    :return: label object
    """
    label = Label(name=label_name, creator_id=user_id)
    session.add(label)
    session.commit()
    return label


@provide_logging
def get_label_by_id(session, user_id, label_id):
    """
    Finds label with id
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param label_id: id of the label where we provides this operation
    :return: label object
    """
    label = session.query(Label).filter(Label.id == label_id).one()
    return label


@provide_logging
def get_label_by_name(session, user_id, label_name):
    """
    Finds Label of user with name
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param label_name: name of the label where we provides this operation
    :return: label object
    """
    label = session.query(Label).filter(Label.name == label_name, Label.creator_id == user_id).one()
    return label


@provide_logging
def get_user_labels(session, user_id):
    """
    Finds all user labels
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    """
    labels = session.query(Label).filter(Label.creator_id == user_id).all()
    return labels


@provide_logging
def get_note_labels(session, user_id, note_id):
    """
    Finds all labels that assigned to note
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where labels is situated
    """
    labels = session.query(NoteLabel).filter(NoteLabel.note_id == note_id).all()
    return labels


@provide_logging
def get_note_label_by_label_id_and_note_id(session, user_id, note_id, label_id):
    """
    Find connection object of note and label
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :param label_id: id of the label where we provides this operation
    :return: NoteLabel object
    """
    note_label = session.query(NoteLabel).filter(and_(NoteLabel.label_id == label_id,
                                                      NoteLabel.note_id == note_id)).one()
    return note_label


@provide_logging
def update_label(session, user_id, label_id, new_name):
    """
    Updates Label data
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param label_id: id of the label where we provides this operation
    :param new_name: new name of the label
    :return: label object
    """
    label = session.query(Label).filter(Label.id == label_id).update({Label.name: new_name},
                                                                     synchronize_session=False)
    return label


@provide_logging
def assign_label_to_note(session, user_id, note_id, label_id):
    """
    Adds Label and Note Connection Object
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :param label_id: id of the label where we provides this operation
    :return: NoteLabel object
    """
    note_label = NoteLabel(note_id=note_id, label_id=label_id)
    session.add(note_label)
    session.commit()
    return note_label


@provide_logging
def remove_label(session, user_id, label_name):
    """
    Removes label from db
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param label_name: id of the label we remove
    :return: True or exception
    """
    label = get_label_by_name(session, user_id, label_name)
    session.delete_all(label.note_labels)
    session.delete(label)
    session.commit()
    return True


@provide_logging
def remove_note_label(session, user_id, note_id, label_id):
    """
    Removes Label and Note Connection
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :param label_id: id of the label where we provides this operation
    :return: true or exception
    """
    note_label = get_note_label_by_label_id_and_note_id(session, user_id, note_id, label_id)
    session.delete(note_label)
    session.commit()
    return True


@provide_logging
def remove_all_note_labels(session, user_id, note_id):
    """
    Removes All Labels of the note
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :return: True or exception
    """
    note_labels = session.query(NoteLabel).filter(NoteLabel.note_id == note_id).all()
    for note_label in note_labels:
        session.delete(note_label)
    session.commit()
    return True


# name was choosed like java programmers names
# this finction in Spring Data DAO
@provide_logging
def find_labels_by_name_like(session, user_id, text):
    """
    Finds label that consists text
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param text: text that contains label name
    :return: labels list
    """
    labels = (session.query(Label).filter(and_(Label.creator_id == user_id,
                                               Label.name.like("%{}%".format(text)))).limit(SEARCH_LIMIT).all())
    return labels

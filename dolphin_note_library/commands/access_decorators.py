"""
    This code provides permission check in operations
    with tracker entities
"""
from functools import wraps

from dolphin_note_library.models.note import (
    Note,
    NoteCollaborator,
)
from dolphin_note_library.models.project import (
    ProjectCollaborator,
    Project,
)


def check_note_permission(func):
    """This decorator provides access checking for notes"""

    @wraps(func)
    def check_and_wrapp(*args, **kwargs):
        session = args[0]
        user_id = args[1]
        note_id = args[2]

        note = session.query(Note).filter(Note.id == note_id).one()
        project = note.project

        # fetching note and project collaborators
        note_collaborators = (session.query(NoteCollaborator)
                                     .filter(NoteCollaborator.note_id == note_id).all())
        project_collaborators = (session.query(ProjectCollaborator)
                                        .filter(ProjectCollaborator.project_id == project.id).all())

        note_collaborators_ids = [
            note_collaborator.collaborator_id
            for note_collaborator in note_collaborators
        ]
        project_collaborators_ids = [
            project_collaborator.member_id
            for project_collaborator in project_collaborators
        ]

        # checks permission
        validation_result = (
                (note.creator_id == user_id) or
                (user_id in note_collaborators_ids) or
                (user_id in project_collaborators_ids)
        )
        if validation_result:
            result = func(*args, **kwargs)
        else:
            raise PermissionError("Can't validate permission to note!")
        return result

    return check_and_wrapp


def check_project_permission(func):
    """This decorator provides access checking for projects"""

    @wraps(func)
    def check_and_wrapp(*args, **kwargs):
        session = args[0]
        user_id = args[1]
        project_id = args[2]

        project = session.query(Project).filter(Project.id == project_id).one()

        # fetching project collaborators
        project_collaborators = (session.query(ProjectCollaborator)
                                        .filter(ProjectCollaborator.project_id == project.id).all())
        project_collaborators_ids = [
            project_collaborator.member_id
            for project_collaborator in project_collaborators
        ]

        # checks permission
        validation_result = (
                (project.creator_id == user_id) or
                (user_id in project_collaborators_ids)
        )
        if validation_result:
            result = func(*args, **kwargs)
        else:
            raise PermissionError("Can't validate permission to project!")
        return result

    return check_and_wrapp

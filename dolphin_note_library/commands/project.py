"""
    This code provides operations with Project entity
"""

from sqlalchemy import and_

from dolphin_note_library.config import SEARCH_LIMIT
from dolphin_note_library.models.project import Project, ProjectCollaborator
from dolphin_note_library.commands.note import remove_all_project_notes
from dolphin_note_library.commands.user import get_user_by_username
from dolphin_note_library.utils.logging import provide_logging
from dolphin_note_library.commands.access_decorators import check_project_permission


@provide_logging
def add_project(session, user_id, project_name):
    """
    Adds project to db
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_name: name of the adding project
    :param project_description: description of the adding project
    :return: project object
    """
    project = Project(name=project_name,
                      creator_id=user_id)
    session.add(project)
    session.commit()
    return project


@provide_logging
@check_project_permission
def get_project_by_id(session, user_id, project_id):
    """
    Finds project with id
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the note where we provides this operation
    :return: project object
    """
    project = session.query(Project).filter(Project.id == project_id).one()
    return project


@provide_logging
def get_all_assigned_projects(session, user_id):
    """
    Finds all prrojects where user assigned
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :return: projects list
    """
    project_collaborators = (session.query(ProjectCollaborator)
                                    .filter(ProjectCollaborator.member_id == user_id).all())
    projects = [session.query(Project)
                       .filter(Project.id == project_collaborator.project_id).one()
                for project_collaborator in project_collaborators]

    return projects


@provide_logging
@check_project_permission
def get_all_project_assignees(session, user_id, project_id):
    """
    Fins all users that assigned to project
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the note where we provides this operation
    :return: User object
    """
    users = session.query(ProjectCollaborator).filter(ProjectCollaborator.project_id == project_id).all()

    return users


@provide_logging
def get_all_user_projects(session, user_id, is_archived=False):
    """
    Finds all user projects with the status archived or not
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param is_archived: archive status of the project
    :return: projects list
    """
    projects = session.query(Project).filter(Project.creator_id == user_id,
                                             Project.is_archived == is_archived)

    return projects


# name was choosed like java programmers names
# this finction in Spring Data DAO
@provide_logging
def find_projects_by_name_like(session, user_id, text):
    """
    Finds all projects that name contains text
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param text: name contains this text
    :return: projects list
    """
    projects = session.query(Project).filter(and_(Project.creator_id == user_id,
                                                  Project.name.like("%{}%".format(text)))).limit(SEARCH_LIMIT).all()
    return projects


@provide_logging
@check_project_permission
def update_project(session,
                   user_id,
                   project_id,
                   project_name=None,
                   project_is_archived=None):
    """
    Updates project fields
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the note where we provides this operation
    :param project_name: new project name
    :param project_description: new project description
    :param project_is_archived: new project archive status
    :return: project object
    """
    update_params = dict()
    if project_name:
        update_params[Project.name] = project_name
    if project_is_archived != None:
        update_params[Project.is_archived] = project_is_archived

    project = session.query(Project).filter(Project.id == project_id)\
                                    .update(update_params)
    session.commit()
    return project


@provide_logging
@check_project_permission
def remove_project_assignee(session, user_id, project_id, assigned_username):
    """
    Removes all User-Project Connections of the ptoject
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the note where we provides this operation
    :param assigned_user_id: id of assigned user
    :return: True or exception
    """
    user = get_user_by_username(session, assigned_username)
    collaborator = (session.query(ProjectCollaborator)
                           .filter(and_(ProjectCollaborator.project_id == project_id,
                                        ProjectCollaborator.member_id == user.id)).first())
    session.delete(collaborator)
    session.commit()
    return True


@provide_logging
@check_project_permission
def remove_all_project_assignations(session, user_id, project_id):
    """
    Removes User and Project connection object
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the note where we provides this operation
    :return: True or exception
    """
    collaborators = (session.query(ProjectCollaborator)
                            .filter(ProjectCollaborator.project_id == project_id).all())
    for collaborator in collaborators:
        session.delete(collaborator)
        session.commit()
    return True


@provide_logging
@check_project_permission
def assign_user_to_project(session, user_id, project_id, username):
    """
    Adds User and Project connection object
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the note where we provides this operation
    :param username: name of the assigning user
    :return: ProjectCollaborator object
    """
    user = get_user_by_username(session, username)
    project_collaborator = ProjectCollaborator(
        creator_id=user_id,
        project_id=project_id,
        member_id=user.id
    )
    session.add(project_collaborator)
    session.commit()
    return project_collaborator


@provide_logging
@check_project_permission
def remove_project(session, user_id, project_id):
    """
    Removes project from db
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the note where we provides this operation
    :return: True or exception
    """
    project = get_project_by_id(session, user_id, project_id)
    remove_all_project_notes(session, user_id, project_id)
    remove_all_project_assignations(session, user_id, project_id)

    session.delete(project)
    session.commit()
    return True

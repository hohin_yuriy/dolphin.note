"""
    This code provides operations with Note entity
"""

from sqlalchemy.sql.elements import and_

from dolphin_note_library.commands.access_decorators import check_note_permission
from dolphin_note_library.commands.access_decorators import check_project_permission
from dolphin_note_library.commands.label import remove_all_note_labels
from dolphin_note_library.commands.user import get_user_by_username
from dolphin_note_library.config import SEARCH_LIMIT
from dolphin_note_library.models.note import (
    Note,
    NoteCollaborator,
    NotePriority,
)
from dolphin_note_library.utils.exceptions import BadUsageException
from dolphin_note_library.utils.logging import provide_logging


@provide_logging
def add_note(
        session,
        user_id,
        name,
        project_id=None,
        parent_note_id=None,
        priority=NotePriority.NORMAL,
        due_date=None
):
    """
    Adds note to db

    You should write only project id if it will be root note
    or parent note id if it will be a subnote
    If you set and project id and parent note id we use info about panrent note

    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param name: name of the adding note
    :param due_date: deadline date of the note
    :param project_id: id of the project where note is situated
    :param parent_note_id: id of the parent note
    :param priority: priority of the note
    :return: note object
    """
    if parent_note_id:
        parent_note = get_note_by_id(session, user_id, parent_note_id)
        project_id = parent_note.project_id
        is_archived = parent_note.is_archived
    elif project_id:
        is_archived = False
    else:
        raise BadUsageException("Incorrect data, dont have info about project or parent note!")

    note = Note(
        name=name,
        priority=priority,
        project_id=project_id,
        parent_note_id=parent_note_id,
        creator_id=user_id,
        due_date=due_date,
        is_archived=is_archived
    )
    session.add(note)
    session.commit()
    return note


@provide_logging
@check_note_permission
def get_note_by_id(session, user_id, note_id):
    """
    Finds note with id in db
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :return: note object
    """
    note = session.query(Note).filter(Note.id == note_id).one()

    return note


@provide_logging
@check_project_permission
def get_all_project_root_notes(session, user_id, project_id):
    """
    Finds all root notes(notes without parents) in db
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the project where we provides this operation
    :return: notes list
    """
    # Note.parent_note_id == None
    # because magic method for is isn't overrided
    notes = session.query(Note).filter(and_(Note.project_id == project_id,
                                            Note.parent_note_id == None)).all()

    return notes


@provide_logging
@check_project_permission
def get_all_project_notes(session, user_id, project_id):
    """
    Returns all project notes as list
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param project_id: id of the project where we provides this operation
    :return: notes list
    """
    notes = session.query(Note).filter(Note.project_id == project_id).all()

    return notes


# name was choosed like java programmers names
# this finction in Spring Data DAO
@provide_logging
def find_notes_by_name_like(session, user_id, text):
    """
    Find notes that name contsins some text
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param text: text that contains note
    :return: notes list
    """
    notes = (session.query(Note)
                    .filter(and_(Note.creator_id == user_id, Note.name.like("%{}%".format(text))))
                    .limit(SEARCH_LIMIT)
                    .all())
    return notes


@provide_logging
@check_note_permission
def update_note(
        session,
        user_id,
        note_id,
        name=None,
        archive=None,
        priority=None,
        project_id=None,
        parent_note_id=None,
        due_date=None
):
    """
    Updates note params
    :param priority: priority of the note, NotePriorityEnum value
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :param name: new name of note
    :param archive: new archive status of note
    :param project_id: id of the project where we provides this operation
    :param parent_note_id: new id of the patent note
    :param due_date: new deadline date
    :return: note object
    """
    note = get_note_by_id(session, user_id, note_id)
    if isinstance(archive, bool):
        note.is_archived = archive
        for subnote in note.subnotes:
            update_note(session, user_id, subnote.id, archive=archive)
            session.commit()
    if name:
        note.name = name
    if project_id:
        note.project_id = project_id
    if parent_note_id:
        parent_note = get_note_by_id(session, user_id, parent_note_id)
        if parent_note:
            note.parent_note_id = parent_note_id
            note.project_id = parent_note.project_id
        else:
            raise BadUsageException("Can't find parent note with id {} to update note with id {}"
                                    .format(note_id, parent_note_id))
    if due_date:
        note.due_date = due_date
    if priority:
        note.priority = priority
    session.add(note)
    session.commit()

    return note


@provide_logging
@check_note_permission
def assign_user_to_note(
        session,
        user_id,
        note_id,
        collaborator_username
):
    """
    Gives assignation to user to work with note
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :param collaborator_username:
    :return: NoteCollaborator object
    """
    collaborator_account = get_user_by_username(session, collaborator_username)
    collaborator = NoteCollaborator(note_id=note_id, collaborator_id=collaborator_account.id, creator_id=user_id)
    session.add(collaborator)
    session.commit()
    return collaborator


@provide_logging
@check_note_permission
def remove_note_assignee(
        session,
        user_id,
        note_id,
        collaborator_id
):
    """
    Deletes assignation to user to work with note
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :param collaborator_id:
    :return: True or exception
    """
    collaborator = (session.query(NoteCollaborator)
                           .filter(and_(NoteCollaborator.collaborator_id == collaborator_id,
                                        NoteCollaborator.note_id == note_id)).first())
    session.delete(collaborator)
    session.commit()
    return True


@provide_logging
@check_note_permission
def remove_all_note_assignees(session, user_id, note_id):
    """
    Deletes all user-note colations
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :return: True or exception
    """
    collaborators = (session.query(NoteCollaborator)
                            .filter(NoteCollaborator.note_id == note_id).all())
    for collaborator in collaborators:
        remove_note_assignee(session, user_id, note_id, collaborator.collaborator_id)
    return True


@provide_logging
@check_note_permission
def remove_note(session, user_id, note_id):
    """
    Deletes note from db with all data
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :return: True or exception
    """
    note = get_note_by_id(session, user_id, note_id)
    remove_all_note_labels(session, user_id, note_id)
    remove_all_note_assignees(session, user_id, note_id)

    for schedule in note.schedulers:
        session.delete(schedule)

    for notification in note.notifications:
        session.delete(notification)

    for subnote in note.subnotes:
        remove_note(session, user_id, subnote.id)
    session.delete(note)
    session.commit()
    return True


@check_project_permission
def remove_all_project_notes(session, user_id, project_id):
    """
    Remove all notes from project
    :param session: object that establishes all conversations with the database
    :param user_id:  id of user that executes this operation
    :param project_id: id of the note where we provides this operation
    :return: True or exception
    """
    notes = session.query(Note).filter(Note.project_id == project_id).all()
    for note in notes:
        remove_note(session, user_id, note.id)
    return True


@provide_logging
@check_note_permission
def get_note_assignees(session, user_id, note_id):
    """
    Findes all executors of note
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :param note_id: id of the note where we provides this operation
    :return: NoteCollaborators list
    """
    note_collaborators = (session.query(NoteCollaborator)
                                 .filter(NoteCollaborator.note_id == note_id).all())
    return note_collaborators


@provide_logging
def get_all_assigned_notes_to_user(session, user_id):
    """
    Finds all assigned notes to user
    :param session: object that establishes all conversations with the database
    :param user_id: id of user that executes this operation
    :return: NoteCollaborators list
    """
    note_collaborators = (session.query(NoteCollaborator)
                                 .filter(NoteCollaborator.collaborator_id == user_id).all())
    notes = [session.query(Note).filter(Note.id == note_collaborator.note_id).one()
             for note_collaborator in note_collaborators]
    return notes
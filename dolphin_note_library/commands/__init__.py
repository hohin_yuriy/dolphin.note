"""
This packege contains functions that helps
to provide CRUD operations with DB tables
of the dolphin.note tracker

Commands package provides CRUD operations with db
    label.py, note.py, notification.py, plan.py,
    project.py, scheduler.py, user.py
    provides operations with Label, Note entity
    and so on

    Example use:
    >>> from dolphin_note_library.commands.project import get_project_by_id
    >>> from dolphin_note_library.database import Database
    >>> session = Database().get_session()
    >>> project = get_project_by_id(session, user_id, project_id)
"""
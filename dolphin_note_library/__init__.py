"""
Dolphin.Note tracker library contains functions and classes
to work with tracker database.
    Database class provides database connection setup
    session object provides connection with database
    to get session object you should:
    >>> database = Database()
    >>> session = database.get_session()

    Models package provides logger with ORM entities
    that help to connect tables in db and
    python objects in tracker.
    They are User, Project, Note, Label, TimeNotification,
    Schedule, NoteTemplate and so on

    common.py:
        * ColumnLength - class with constants,
        that we user in VARCHAR columns to
        set its length

    Commands package provides CRUD operations with db
    label.py, note.py, notification.py, plan.py,
    project.py, scheduler.py, user.py
    provides operations with Label, Note entity
    and so on

    Example use:
        Example use:
    >>> from dolphin_note_library.commands.project import get_project_by_id
    >>> from dolphin_note_library.database import Database
    >>> session = Database().get_session()
    >>> project = get_project_by_id(session, user_id, project_id)

    Access decorators check_note_permission and check_project_permission
    detects if user is creator or assignee and allows to provide the operation

    If you want to use this decorators with your custom commands
    you should have session and user_id as first two params
    and
    >>> @check_note_permission
    >>> def some_command(session, user_id, *args, *kvargs):
    ... # some code hear

    or use

    >>> @check_project_permission



    config.py - application constants


Utils folder consists of:
    - logging.py:
      consists of
      * catch_exceptions_and_log decorator that provides
      logging and catching exceptions in commands
      Use:
      >>> @provide_logging

      * configure_logging(logger_name, level, log_directory, log_format, log_filename)
        Allows to set logger configuration to the logger with logger_name


    - exceptions.py:
    consists of custom Exceptions for tracker library
    BadUsageException - provides raising exceprions
    linked with incorrect data input
    Use:
    >>> raise BadUsageException(TEXT)

    - datetime.py
    consists of some usefull functions
    to easy work with datetime objects

    * get_datetime_from_string:
    Use timestring library to transer
    string with datetime in a human format
    to datetime object

    * DeltaType
    enum, use to set time period in weeks, days, months
    with get_relative_delta

    * get_relative_delta(delta_type, quantity)
    transfers DeltaType enum and quantity to
    relativedelta object

"""


__version__ = '0.0.1'
__all__ = [
    'commands',
    'utils',
    'config',
    'database',
    'models',
    'setup'

]

__author__ = 'Hohin Yuri'


from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    DateTime,
    ForeignKey,
    String,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship, backref

from dolphin_note_library.models.base import GenericModel
from dolphin_note_library.models.user import User
from dolphin_note_library.models.note import Note
from dolphin_note_library.models.common import ColumnLength


class Label(GenericModel):
    """
        Represents model of the Label in Dolphin note tracker
    """
    __tablename__ = "label"
    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship(User)
    updation_date = Column(DateTime, onupdate=datetime.now)
    name = Column(String(ColumnLength.BEHIND_LARGE_AND_MIDDLE))
    color = Column(String(7), default="FF0000")
    note_labels = relationship(
        "NoteLabel",
        backref=backref(
            "label",
            single_parent=True
        )
    )

    __table_args__ = (
        UniqueConstraint(
            'creator_id',
            'name',
            name='uncn_1'
        ),
    )
    __mapper_args__ = {'concrete': True}


class NoteLabel(GenericModel):
    """
        Represents relation between Note and Label
    """
    __tablename__ = "note_label"
    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship(User)
    updation_date = Column(DateTime, onupdate=datetime.now)
    label_id = Column(Integer, ForeignKey("label.id"))
    label = relationship(Label)
    note_id = Column(Integer, ForeignKey("note.id"))
    note = relationship(Note)

    __table_args__ = (
        UniqueConstraint(
            'note_id',
            'label_id',
            name='uncc_1'
        ),
    )
    __mapper_args__ = {'concrete': True}

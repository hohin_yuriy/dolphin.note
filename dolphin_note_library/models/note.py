import enum
from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    ForeignKey,
    Enum,
    Boolean,
    UniqueConstraint,
    Date,
)
from sqlalchemy.orm import (
    relationship,
    backref,
)

from dolphin_note_library.models.base import (
    GenericModel,
    DataTypeModel,
)
from dolphin_note_library.models.common import ColumnLength
from dolphin_note_library.models.project import Project
from dolphin_note_library.models.schedule import Schedule
from dolphin_note_library.models.user import User
from dolphin_note_library.models.notification import TimeNotification


class NotePriority(enum.Enum):
    """Class represents degrees of note priority"""
    HIGH = "High"
    IMPORTANT = "Important"
    NORMAL = "Normal"
    LOW = "Low"


class Note(DataTypeModel):
    """
        Represent's of note in the tracker
    """
    __tablename__ = "note"
    id = Column(Integer, primary_key=True)
    name = Column(String(ColumnLength.BEHIND_LARGE_AND_MIDDLE), nullable=False)

    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship(User)
    updation_date = Column(DateTime, onupdate=datetime.now)

    project_id = Column(Integer, ForeignKey("project.id"))
    project = relationship(Project)

    priority = Column(Enum(NotePriority), default=NotePriority.NORMAL)

    due_date = Column(Date, default=None)

    is_archived = Column(Boolean, default=False, nullable=False)

    parent_note_id = Column(Integer, ForeignKey("note.id"))
    parent_note = relationship("Note", remote_side=[id])
    subnotes = relationship(
        "Note",
        backref=backref(
            'parent_note',
            remote_side=[id],
            single_parent=True
        )
    )

    collaborators = relationship(
        "NoteCollaborator",
        backref=backref(
            "note",
            single_parent=True
        )
    )

    notifications = relationship(
        TimeNotification,
        backref=backref(
            "note",
            single_parent=True
        )
    )

    schedulers = relationship(
        Schedule,
        backref=backref(
            "note",
            single_parent=True
        )
    )

    note_labels = relationship(
        "NoteLabel",
        backref=backref(
            "note",
            single_parent=True
        )
    )

    __mapper_args__ = {'concrete': True}

    @property
    def has_childs(self):
        if self.subnotes:
            return True
        else:
            return False

    @property
    def get_priority_color(self):
        if self.priority == NotePriority.LOW:
            return "#DDEDC2"
        elif self.priority == NotePriority.NORMAL:
            return "#FFD4B5"
        elif self.priority == NotePriority.IMPORTANT:
            return "#FFABA6"
        elif self.priority == NotePriority.HIGH:
            return "#FF8C94"


class NoteCollaborator(GenericModel):
    """
        Represents relation between Note and User
    """
    __tablename__ = "note_collaborator"
    __table_args__ = (
        UniqueConstraint(
            'note_id',
            'collaborator_id',
            name='uncc_1'
        ),
    )

    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship(User, foreign_keys=[creator_id])
    updation_date = Column(DateTime, onupdate=datetime.now)
    note_id = Column(Integer, ForeignKey("note.id"))
    note = relationship(Note)
    collaborator_id = Column(Integer, ForeignKey("account.id"))
    collaborator = relationship(User, foreign_keys=[collaborator_id])

    __mapper_args__ = {'concrete': True}

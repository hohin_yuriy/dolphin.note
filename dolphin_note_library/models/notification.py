from datetime import datetime, timedelta

from sqlalchemy import (
    Column,
    Integer,
    DateTime,
    ForeignKey,
    Interval,
    Date,
)
from sqlalchemy.orm import relationship

from dolphin_note_library.models.base import GenericModel
from dolphin_note_library.models.user import User


class TimeNotification(GenericModel):
    """
        Represents model of the Time Notification
        in Dolphin note tracker
    """
    __tablename__ = "time_notification"
    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship(User)
    updation_date = Column(DateTime, onupdate=datetime.now)
    date = Column(Date, nullable=False)
    delta = Column(Interval, default=timedelta(days=1))

    note_id = Column(Integer, ForeignKey("note.id"))
    note = relationship("Note")

    __mapper_args__ = {'concrete': True}

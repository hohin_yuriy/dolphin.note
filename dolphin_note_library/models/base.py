from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    DateTime,
    ForeignKey,
    String,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from dolphin_note_library.models.common import ColumnLength

Base = declarative_base()


class GenericModel(Base):
    """
        This class represents common columns of every model
        Base object for concrete tables inheritance
        For more information about inheritance:
        http://docs.sqlalchemy.org/en/latest/orm/extensions/declarative/inheritance.html
    """
    __tablename__ = 'generic'
    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship("User")
    updation_date = Column(DateTime, onupdate=datetime.now)


class DataTypeModel(GenericModel):
    """
        This class represents common columns of every data model
    """
    __tablename__ = 'data_type_model'
    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship("User")
    updation_date = Column(DateTime, onupdate=datetime.now)
    name = Column(String(ColumnLength.BEHIND_LARGE_AND_MIDDLE), nullable=False)

    __mapper_args__ = {'concrete': True}
from datetime import (
    datetime,
    timedelta,
)

from sqlalchemy import (
    Column,
    Integer,
    ForeignKey,
    DateTime,
    Interval,
)
from sqlalchemy.orm import relationship

from dolphin_note_library.models.base import GenericModel


class Schedule(GenericModel):
    """
        Represents repeateble adder of notes
    """
    __tablename__ = "schedule"
    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    updation_date = Column(DateTime, onupdate=datetime.now)

    delta = Column(Interval, default=timedelta())

    note_id = Column(Integer, ForeignKey("note.id"))
    note = relationship("Note")

    start_date = Column(DateTime, nullable=False)
    end_date = Column(DateTime, nullable=True)
    last_created = Column(DateTime, nullable=False, default=start_date)

    __mapper_args__ = {'concrete': True}
from sqlalchemy import (
    Column,
    Integer,
    String,
)

from dolphin_note_library.models.base import Base
from dolphin_note_library.models.common import ColumnLength


class User(Base):
    """
        This class represents account model for dolphin_note_cli application
    """
    __tablename__ = "account"
    id = Column(Integer, primary_key=True)
    username = Column(String(ColumnLength.VERY_LARGE), unique=True)
    email = Column(String(ColumnLength.VERY_LARGE))

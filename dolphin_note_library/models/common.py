class ColumnLength:
    """Class represents resources length for models"""
    VERY_LARGE = 255
    LARGE = 100
    BEHIND_LARGE_AND_MIDDLE = 50
    MIDDLE = 5
    SHORT = 3
    LOW = 1
    NAME_LENGTH = 30



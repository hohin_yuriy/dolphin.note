from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    ForeignKey,
    Boolean,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship, backref

from dolphin_note_library.models.base import (
    GenericModel,
    DataTypeModel,
)
from dolphin_note_library.models.common import ColumnLength


class Project(DataTypeModel):
    """
        Represent's project model of the tracker
    """
    __tablename__ = 'project'
    id = Column(Integer, primary_key=True)
    name = Column(String(ColumnLength.BEHIND_LARGE_AND_MIDDLE), nullable=False)
    description = Column(String(ColumnLength.VERY_LARGE), default="", nullable=False)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship("User")
    collaborators = relationship("ProjectCollaborator", backref=backref("project", single_parent=True))
    updation_date = Column(DateTime, onupdate=datetime.now)
    is_archived = Column(Boolean, default=False)
    __mapper_args__ = {'concrete': True}


class ProjectCollaborator(GenericModel):
    """
        Represents relation between User and Project
    """
    __tablename__ = "project_collaborator"
    __table_args__ = (UniqueConstraint('project_id', 'member_id', name='upmc_1'),)

    id = Column(Integer, primary_key=True)
    creation_date = Column(DateTime, nullable=False, default=datetime.now)
    creator_id = Column(Integer, ForeignKey("account.id"))
    creator = relationship("User", foreign_keys=[creator_id])
    updation_date = Column(DateTime, onupdate=datetime.now)
    project_id = Column(Integer, ForeignKey("project.id"))
    project = relationship(Project, foreign_keys=[project_id])
    member_id = Column(Integer, ForeignKey("account.id"))
    member = relationship("User", foreign_keys=[member_id])

    __mapper_args__ = {'concrete': True}
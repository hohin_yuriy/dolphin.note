from sqlalchemy import create_engine
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
)

from dolphin_note_library.config import DATABASE_CONFIG
from dolphin_note_library.models.base import Base


class Database(object):
    """
        This class sets database connection settings
        and gets connection to the db
    """
    def __init__(
            self,
            is_memory=False,
            connection_string=None,
            db_file_name=None
    ):
        """Connects dolphin_note_library"""
        if not is_memory:
            if connection_string:
                self.engine = create_engine(connection_string, echo=False)
            else:
                connection_string = self.get_connection_string()
                self.engine = create_engine(connection_string, echo=False)
        else:
            if db_file_name:
                connection_string = 'sqlite:///' + db_file_name
            else:
                connection_string = "sqlite:///:memory:"
            self.engine = create_engine(connection_string, echo=False)

        Session = scoped_session(
            sessionmaker(
                bind=self.engine,
                expire_on_commit=False
            )
        )
        self.session = Session()

        self.parse_tables()

    def parse_tables(self):
        """Create tables"""
        Base.metadata.create_all(self.engine)

    def reset_db(self):
        """Create tables"""
        Base.metadata.drop_all(self.engine)
        Base.metadata.create_all(self.engine)

    def get_connection_string(self):
        """
        This method helps to get a string for SQLAlchemy
        engine to make a conection with dolphin_note_library
        Database URLs should be of the form:
        driver://user[:password]@host[:port]/database
        """
        connection_string = (
                DATABASE_CONFIG['driver'] +
                "://" +
                DATABASE_CONFIG['user'] +
                ":" +
                DATABASE_CONFIG['password'] +
                "@" +
                DATABASE_CONFIG['host'] +
                ":" +
                DATABASE_CONFIG['port'] +
                "/" +
                DATABASE_CONFIG['dbname']
        )

        return connection_string

    def get_session(self):
        """
        :return: returns session object
        """
        return self.session

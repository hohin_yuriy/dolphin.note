# -*- coding: UTF-8 -*-

"""
    This module configure dolphin_note_library library of the project

    (c) Hohin Yuri, 2018
"""

import os

# gets user home directory

# app resources
APP_NAME = "dolphin.note"
VERSION = "0.0.2"
DESCRIPTION = "This is task tracker for lazy dolphins!"

APP_HOME = os.path.dirname(os.path.abspath(__file__))


# module resources
CORE_MODULE_NAME = "dolphin_note_library"
CORE_MODULE_PATH = APP_HOME

# logging resources
LOGGER_NAME = "DOLPHIN_NOTE_SUPER_PUPER_DUPER_LOGGER"
LOG_FORMAT = ("(%(process)d/%(threadName)s) "
              "Dolph.Note:%(levelname)s:%(message)s:(%(asctime)s)")

LOG_FILE_FOLDER_NAME = "log"
LOG_DIRECTORY = os.path.join(CORE_MODULE_PATH, LOG_FILE_FOLDER_NAME)
print(LOG_DIRECTORY)

LOG_LEVEL = "ERROR"


# data source resources
DATABASE_PLATFORM = "MYSQL"

# core resources for mysql, ma be changed to sqlite
# root user is bad but I'm lazy to create a db user for this app:))
DATABASE_CONFIG = {
    'driver': 'mysql+pymysql',
    'host': 'localhost',
    'dbname': 'dolfin',
    'user': 'root',
    'password': '461108',
    'port': '3306'
}


# limit for SQLAlchemy live searchs
SEARCH_LIMIT = 5

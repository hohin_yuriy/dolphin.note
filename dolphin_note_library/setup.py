import os

from setuptools import setup


def read(file_name):
    return open(os.path.join(os.path.dirname(__file__), file_name)).read()


setup(
    name="dolphin note library",
    version="2.0.1",
    author="Yuriy Hohin",
    author_email="yura.gogin@gmail.com",
    description="A very usefull task tracker library for Unicorns)))",
    license="EULA",
    keywords="task tracker note project",
    url="",
    packages=['commands', 'utils'],
    install_requires=["Click", "sqlalchemy", "datetime", 'timestring'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
